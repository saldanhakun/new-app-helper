<?php

namespace Saldanhakun\AppHelper\Workflow;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\Marking;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AppWorkflow implements EventSubscriberInterface
{

    protected $workflow;
    private $subject;
    protected $messageBus;
    protected $translator;
    protected $entityManager;
    protected $eventDispatcher;
    protected $mailer;

    public function __construct(MessageBusInterface $messageBus, TranslatorInterface $translator, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, MailerInterface $mailer)
    {
        $this->messageBus = $messageBus;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->mailer = $mailer;
    }

    protected function setWorkflow(WorkflowInterface $workflow) {
        $this->workflow = $workflow;
    }

    protected function trans($message, array $params=[]): string
    {
        return $this->translator->trans($message, $params);
    }

    protected function failIfNoSubject(): void
    {
        if ($this->subject === null) {
            throw new \LogicException($this->trans('workflow.noSubjectError'));
        }
    }

    protected function failIfNotAllowed(string $transition): void
    {
        $this->failIfNoSubject();
        if (!$this->workflow->can($this->getSubject(), $transition)) {
            throw new LogicException($this->trans('workflow.transitionNotAllowedError', [
                '{{ current }}' => $this->getCurrentState(),
                '{{ transition }}' => $transition,
            ]));
        }
    }

    public function getSubject(): ?object
    {
        return $this->subject;
    }

    public function setSubject(?object $subject): self
    {
        if ($subject && !static::acceptSubject($subject)) {
            throw new \LogicException(sprintf("Subject not accepted for %s: %s", get_class($this), get_class($subject)));
        }
        $this->subject = $subject;
        return $this;
    }

    /**
     * Verifica se a implementação do Workflow aceita manipular o objeto sinalizado em um evento de workflow
     * @param object $subject
     * @return bool
     */
    abstract public static function acceptSubject(object $subject): bool;

    /**
     * @return Marking O estado atual do workflow
     */
    public function getCurrent(): Marking
    {
        $this->failIfNoSubject();
        return $this->workflow->getMarking($this->getSubject());
    }

    /**
     * @return string O estado atual
     */
    public function getCurrentState(): string
    {
        // A Validação de Status é uma Máquina de Estado, e portanto não permite 2 estados simultâneos.
        // Ainda assim, usamos a cola por garantia e compatibilidade.
        return implode('+', array_keys(array_filter($this->getCurrent()->getPlaces())));
    }

    /**
     * @return Transition[] As transições disponíveis neste ponto do workflow
     */
    public function getAvailableTransitions(): array
    {
        $this->failIfNoSubject();
        return $this->workflow->getEnabledTransitions($this->getSubject());
    }

    /**
     * Verifica se o passo é permitido
     * @param array $transitions
     * @param bool $andLogic AND=>Exige todas as transições permitidas; OR => Qualquer uma delas já basta.
     * @return bool
     */
    public function can(array $transitions, bool $andLogic = false): bool
    {
        $this->failIfNoSubject();
        $can = $andLogic;
        foreach ($transitions as $transition) {
            if ($this->workflow->can($this->getSubject(), $transition)) {
                if (!$andLogic) {
                    $can = true;
                    break;
                }
            }
            else {
                if ($andLogic) {
                    $can = false;
                    break;
                }
            }
        }
        return $can;
    }

    /**
     * Sinaliza o passo executado com sucesso
     * @param string $transition
     */
    protected function apply(string $transition) {
        $this->failIfNotAllowed($transition);
        $this->workflow->apply($this->getSubject(), $transition);
    }

    /**
     * Chama os métodos on* deste workflow para lidar com mudanças no workflow vinculado.
     * Em casos especiais, o workflow pode não ter sido corretamente atribuído, e nesse caso
     * as chamadas não são realizadas, por não ter como saber de qual workflow estamos falando.
     * @param Event $event
     * @param string $handle
     */
    private function delegate(Event $event, string $handle)
    {
        if (method_exists($this, $handle)) {
            if ($this->workflow && $event->getWorkflowName() === $this->workflow->getName()) {
                if (static::acceptSubject($event->getSubject())) {
                    $this->setSubject($event->getSubject());
                    $this->$handle($event);
                }
            }
        }
    }

    /**
     * Identifica a intenção de fazer a transição e dispara eventuais gatilhos internos
     * @param Event $event
     */
    public function watchBeforeTransition(Event $event)
    {
        if ($event->getTransition()) {
            $this->delegate($event, 'onBefore' . ucfirst($event->getTransition()->getName()));
        }
        $this->delegate($event, 'onBefore');
    }

    /**
     * Identifica a conclusão da transição e dispara eventuais gatilhos internos
     * @param Event $event
     */
    public function watchAfterTransition(Event $event)
    {
        if ($event->getTransition()) {
            $this->delegate($event, 'onAfter' . ucfirst($event->getTransition()->getName()));
        }
        $this->delegate($event, 'onAfter');
    }

    /**
     * Identifica a verificação para seguir em frente em uma transição e dispara eventuais gatilhos internos
     * @param GuardEvent $event
     */
    public function watchGuardTransition(GuardEvent $event)
    {
        try {
            if ($event->getTransition()) {
                $this->delegate($event, 'onGuard' . ucfirst($event->getTransition()->getName()));
            }
            $this->delegate($event, 'onGuard');
        }
        catch (\Exception $e) {
            $event->setBlocked(true, $e->getMessage());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.guard' => 'watchGuardTransition',
            'workflow.transition' => 'watchBeforeTransition',
            'workflow.completed' => 'watchAfterTransition',
        ];
    }

}