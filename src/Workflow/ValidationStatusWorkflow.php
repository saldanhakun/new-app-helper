<?php

namespace Saldanhakun\AppHelper\Workflow;

use Saldanhakun\AppHelper\DBAL\ValidationStatusType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ValidationStatusWorkflow extends AppWorkflow
{

    public const ACTION_START = 'start';
    public const ACTION_SUBMIT = 'submit';
    public const ACTION_CONFIRM = 'confirm';
    public const ACTION_EXPIRE = 'expire';
    public const ACTION_CANCEL = 'cancel';
    public const ACTION_BLOCK = 'block';
    public const ACTION_RESTART = 'restart';

    public function __construct(WorkflowInterface $validationStatusStateMachine, MessageBusInterface $messageBus, TranslatorInterface $translator, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, MailerInterface $mailer)
    {
        parent::__construct($messageBus, $translator, $entityManager, $eventDispatcher, $mailer);
        $this->setWorkflow($validationStatusStateMachine);
    }

    /**
     * Verifica se a implementação do Workflow aceita manipular o objeto sinalizado em um evento de workflow
     * @param object $subject
     * @return bool
     */
    public static function acceptSubject(object $subject): bool
    {
        return $subject instanceof UserInterface;
    }

    /**
     * Para validações que controlam o tempo de expiração, permite a verificação do prazo antes da
     * confirmação.
     * @return bool Se true, o sistema faz a mudança para o estado expirado.
     */
    public function hasExpired(): bool
    {
        return false;
    }

    /**
     * @return bool Se o fluxo exige um estágio de validação manual
     */
    public function requiresModeration(): bool
    {
        return false;
    }

    /**
     * @return bool Se o fluxo exige que um arquivo seja enviado para moderação
     */
    public function requiresSubmitAttachment(): bool
    {
        return false;
    }

    /**
     * @return bool Se o fluxo exige uma justificativa no caso de negação da moderação
     */
    public function requiresBlockComment(): bool
    {
        return false;
    }

    /**
     * @return bool Se o fluxo exige uma justificativa no caso de confirmaçõa após moderação
     */
    public function requiresConfirmComment(): bool
    {
        return false;
    }

    /**
     * @return string Rótulo do arquivo solicitado
     */
    public function attachmentTitle(): string
    {
        return $this->trans("workflow.attachmentTitle");
    }

    /**
     * @return bool Verifica se existe um arquivo anexado
     */
    protected function checkAttachment(): bool
    {
        return false;
    }

    /**
     * @return bool Verifica se existe uma justificativa disponível
     */
    protected function checkComment(): bool
    {
        return false;
    }

    protected function onGuardSubmit()
    {
        if (!$this->requiresModeration()) {
            // Não permite!
            throw new \LogicException($this->trans("workflow.noModerationError"));
        }
        if ($this->requiresSubmitAttachment() && !$this->checkAttachment()) {
            // Não permite!
            throw new \LogicException($this->trans("workflow.noAttachmentError"));
        }
        // Para os processos que possuem uma janela de tempo, levanta um erro aqui.
        // Tem o efeito de indicar que a ação não é permitida no método can()
        if ($this->hasExpired()) {
            // Não permite!
            throw new \LogicException($this->trans("workflow.expiredError"));
        }
    }

    protected function onGuardConfirm()
    {
        // Para os processos que possuem uma janela de tempo, levanta um erro aqui.
        // Tem o efeito de indicar que a ação não é permitida no método can()
        if ($this->requiresModeration()) {
            if ($this->requiresConfirmComment() && !$this->checkComment()) {
                // Não permite!
                throw new \LogicException($this->trans("workflow.noCommentError"));
            }
        }
        else {
            if ($this->hasExpired()) {
                // Não permite!
                throw new \LogicException($this->trans("workflow.expiredError"));
            }
        }
    }

    protected function onGuardBlock(Event $event)
    {
        if ($this->requiresModeration()) {
            if ($this->requiresBlockComment() && !$this->checkComment()) {
                // Não permite!
                throw new \LogicException($this->trans("workflow.noCommentError"));
            }
        }
    }

    /**
     * Reinicia o workflow, voltando ao estado inicial de pendente
     * @return bool sucesso
     */
    public function restart(): bool
    {
        // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
        $this->apply(self::ACTION_RESTART);
        return $this->getCurrentState() === ValidationStatusType::STATE_PENDING;
    }

    /**
     * Inicia o workflow, partindo do estado inicial
     * @return bool sucesso
     */
    public function start(): bool
    {
        // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
        $this->apply(self::ACTION_START);
        return $this->getCurrentState() === ValidationStatusType::STATE_INITIATED;
    }

    /**
     * Confirma o envio para moderação
     * @return bool sucesso
     */
    public function submit(): bool
    {
        if ($this->requiresModeration() && $this->hasExpired()) {
            $this->apply(self::ACTION_EXPIRE);
        }
        else {
            // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
            $this->apply(self::ACTION_SUBMIT);
        }
        return $this->getCurrentState() === ValidationStatusType::STATE_SUBMITTED;
    }

    /**
     * Confirma a validação em curso
     * @return bool sucesso
     */
    public function confirm(): bool
    {
        if (!$this->requiresModeration() && $this->hasExpired()) {
            $this->apply(self::ACTION_EXPIRE);
        }
        else {
            // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
            $this->apply(self::ACTION_CONFIRM);
        }
        return $this->getCurrentState() === ValidationStatusType::STATE_CONFIRMED;
    }

    /**
     * Expira a validação não concluída a tempo
     * @return bool sucesso
     */
    public function expire(): bool
    {
        // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
        $this->apply(self::ACTION_EXPIRE);
        return $this->getCurrentState() === ValidationStatusType::STATE_EXPIRED;
    }

    /**
     * Cancela a validação
     * @return bool sucesso
     */
    public function cancel(): bool
    {
        // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
        $this->apply(self::ACTION_CANCEL);
        return $this->getCurrentState() === ValidationStatusType::STATE_CANCELLED;
    }

    /**
     * Bloqueia a validação, sinalizando uma conclusão negativa
     * @return bool sucesso
     */
    public function block(): bool
    {
        // Se permitido, vai disparar eventos que usamos para manipular o objeto de acordo.
        $this->apply(self::ACTION_BLOCK);
        return $this->getCurrentState() === ValidationStatusType::STATE_BLOCKED;
    }

}