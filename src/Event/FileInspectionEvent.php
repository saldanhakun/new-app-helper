<?php

namespace Saldanhakun\AppHelper\Event;

use Saldanhakun\AppHelper\Service\FileInspectionResult;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * O evento user.file.inspection é disparado sempre que uma URL externa precisa ser avaliada
 * quanto ao teor do seu conteúdo.
 */
class FileInspectionEvent extends Event
{
    public const NAME = 'user.file.inspection';

    private $url;
    private $method;
    private $result;
    private $followRedirect;

    public function __construct(string $url, string $method='GET', ?int $followRedirects = null)
    {
        $this->url = $url;
        $this->method = $method;
        $this->followRedirect = $followRedirects;
    }

    /**
     * @return string A URL a ser inspecionada
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string Método HTTP para a consulta
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return FileInspectionResult
     */
    public function getResult(): FileInspectionResult
    {
        return $this->result;
    }

    /**
     * @param FileInspectionResult $result
     */
    public function setResult(FileInspectionResult $result): self
    {
        $this->result = $result;
        return $this;
    }

    public function getFollowRedirect(): bool
    {
        return $this->followRedirect > 0;
    }

    public function getRemainingRedirects(): int
    {
        return $this->followRedirect;
    }

}