<?php

namespace Saldanhakun\AppHelper\Entity;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class AppRole
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SECURITY = 'ROLE_SECURITY';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public const ROLE_ALLOWED_TO_SWITCH = 'ROLE_ALLOWED_TO_SWITCH';
    public const SWITCH_NAME = '_ghost'; /** @see security.yaml */

    public const ROLES = [
        self::ROLE_USER,
        self::ROLE_ADMIN,
        self::ROLE_SECURITY,
        self::ROLE_SUPER_ADMIN,
    ];

    /**
     * @return string[] Lista de identificadores válidos para níveis de segurança
     */
    public static function roles(): array
    {
        return self::ROLES;
    }

    /**
     * @return string Nome legível do nível
     */
    public static function roleName(string $role): string
    {
        return "security.roles.$role.name";
    }

    /**
     * @return string Descrição humana do nível
     */
    public static function roleDescription(string $role): string
    {
        return "security.roles.$role.description";
    }

    /**
     * @return string[] Lista de identificadores válidos para níveis de segurança, com nome legível
     */
    public static function rolesWithName(): array
    {
        $list = [];
        foreach (self::roles() as $role) {
            $list[$role] = "security.roles.$role.name";
        }
        return $list;
    }

    /**
     * A lista de níveis não acessíveis ao usuário num contexto de manipulação do acesso.
     * Se o nível está presente nessa lista, qualquer adição ou remoção dele deve ser ignorada por segurança.
     * @param Security $security
     * @return string[]
     */
    public static function forbiddenRoles(AuthorizationCheckerInterface $security): array
    {
        if ($security->isGranted(self::ROLE_SUPER_ADMIN)) {
            // pode tudo! Mas ROLE_USER é um caso perdido.
            $list = [self::ROLE_USER];
        }
        elseif ($security->isGranted(self::ROLE_SECURITY)) {
            // Não pode mexer com superadmins
            $list = [self::ROLE_USER, self::ROLE_SUPER_ADMIN];
        }
        else {
            // Não pode mexer com segurança
            $list = self::roles();
        }
        return $list;
    }

    /**
     * Retorna a lista completa de níveis conforme a dependência entre eles
     * @param string $role
     * @param bool $includeSelf
     * @return string[]
     */
    public static function roleDependencies(string $role, bool $includeSelf = true): array
    {
        $includes = $includeSelf ? [$role] : [];
        if ($role === self::ROLE_SUPER_ADMIN) {
            $includes = array_merge($includes, self::roleDependencies(self::ROLE_SECURITY));
        }
        elseif ($role === self::ROLE_SECURITY) {
            $includes = array_merge($includes, self::roleDependencies(self::ROLE_ALLOWED_TO_SWITCH));
            $includes = array_merge($includes, self::roleDependencies(self::ROLE_ADMIN));
        }
        elseif ($role === self::ROLE_ADMIN) {
            $includes = array_merge($includes, self::roleDependencies(self::ROLE_USER));
        }
        return array_unique($includes);
    }

    /**
     * Compila a lista expandida de níveis, considerando todas as dependências na cadeia
     * @param string[] $roleList
     * @return string[]
     */
    public static function solveDependencies(array $roleList): array
    {
        $list = [];
        foreach ($roleList as $role) {
            $list = array_merge($list, self::roleDependencies($role, true));
        }
        return array_unique($list);
    }

}