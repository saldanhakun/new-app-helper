<?php

namespace Saldanhakun\AppHelper\Entity;

use App\Entity\UserFile;use Doctrine\Common\Collections\Collection;use Doctrine\ORM\EntityManagerInterface;use Symfony\Component\Security\Core\User\UserInterface;

class UserProfile extends AppEntity implements UserInterface
{

    private $id;
    private $roles;
    private $email;
    private $displayName;
    private $fullName;
    private $document;
    private $mobile;
    private $files = null;
    private $em;

    public function __construct(array $data, EntityManagerInterface $entityManager)
    {
        $this->id = $data['id'];
        $this->roles = $data['roles'];
        $this->email = $data['email'];
        $this->displayName = $data['displayName'];
        $this->fullName = $data['fullName'];
        $this->document = $data['document'];
        $this->mobile = $data['mobile'];
        $this->em = $entityManager;
    }

    protected function getDescriptiveProperties(): array
    {
        return ['displayName'];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->id;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->getEmail();
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = AppRole::ROLE_USER;

        return array_unique($roles);
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): string
    {
        return (string) $this->email;
    }

    public function getObfuscatedEmail(): string
    {
        return self::obfuscateEmail($this->getEmail());
    }

    /**
     * @param string $email
     * @return string Versão obfuscada do e-mail (ex: s******a@n******s.com.br)
     */
    public static function obfuscateEmail(string $email): string
    {
        $parts = explode('@', $email);
        foreach ($parts as $p => $v) {
            $subparts = explode('.', $v);
            foreach ($subparts as $i => $s) {
                if (strlen($s) > 3) {
                    $subparts[$i] = $s[0] . str_repeat('*', strlen($s)-2) . $s[strlen($s) - 1];
                }
                elseif ($p === 0) {
                    // ofusca o username, mesmo se for curtinho
                    $subparts[$i] = str_repeat('*', strlen($s));
                }
            }
            $parts[$p] = implode('.', $subparts);
        }
        return implode('@', $parts);
    }


    public function getDisplayName(): string
    {
        return (string) $this->displayName;
    }

    public function getFullName(): string
    {
        return (string) $this->fullName;
    }

    public function getDocument(): string
    {
        return (string) $this->document;
    }

    public function getMobile(): string
    {
        return (string) $this->mobile;
    }

    /**
     * @return Collection|UserFile[]
     */
    public function getFiles(): Collection
    {
        if ($this->files === null) {
            // Carrega os arquivos sob demanda.
            $this->files = $this->em->getRepository(UserFile::class)->findBy(['userId' => $this->id]);
        }
        return $this->files;
    }

}
