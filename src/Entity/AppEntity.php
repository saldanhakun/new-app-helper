<?php

namespace Saldanhakun\AppHelper\Entity;

use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AppEntity
{
    protected static $stringifier;
    protected static $translator;

    public static function getStringifier(): \Closure
    {
        return static::$stringifier;
    }
    public static function setStringifier(\Closure $callback): void
    {
        static::$stringifier = $callback;
    }

    public static function getTranslator(): ?TranslatorInterface
    {
        return static::$translator;
    }
    public static function setTranslator(?TranslatorInterface $translator): void
    {
        static::$translator = $translator;
    }
    protected function maybeTranslate($message, $params=[], $domain='messages'): string
    {
        if (self::$translator) {
            return self::$translator->trans($message, $params, $domain);
        }
        else {
            return strtr($message, $params);
        }
    }

    /**
     * @return string[] Lista de propriedades capazes de descrever o objeto
     */
    protected function getDescriptiveProperties(): array
    {
        return ['name', 'title'];
    }

    /**
     * @return string[] Lista de propriedades capazes de identificar o objeto
     */
    protected function getIdentifiableProperties(): array
    {
        return ['id', 'uuid'];
    }

    /**
     * @return string Conversão padronizada de objeto para String
     * Pode ser reescrita na classe para uma personalização maior
     */
    protected function stringify(): string
    {
        if (static::$stringifier instanceof \Closure) {
            return (string) static::$stringifier->call($this, $this);
        }
        foreach ($this->getDescriptiveProperties() as $property) {
            $getter = 'get' . ucfirst($property);
            if (method_exists($this, $getter)) {
                return (string) $this->$getter();
            }
        }
        $what = get_class($this);
        foreach ($this->getIdentifiableProperties() as $property) {
            $getter = 'get' . ucfirst($property);
            if (method_exists($this, $getter)) {
                return sprintf('%s #%s', $what, $this->$getter());
            }
        }
        return $what;
    }

    /**
     * @return string Implementação padrão para um __toString.
     * Pode ser reescrita na classe para uma personalização maior
     */
    public function __toString(): string
    {
        return $this->stringify();
    }

}