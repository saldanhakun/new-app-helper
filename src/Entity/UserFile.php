<?php

namespace Saldanhakun\AppHelper\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Saldanhakun\AppHelper\DBAL\FileSourceType;
use Saldanhakun\AppHelper\Repository\UserProfileRepository;
use Saldanhakun\AppHelper\Service\FileSizeConverter;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ ORM\Entity(repositoryClass=UserFileRepository::class)
 * @ ORM\Table(name="user_file",indexes={@ Index(name="user_idx", columns={"userId"})})
 */
trait UserFile
{

    private $tempUploads = [];

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Uuid()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\Uuid()
     */
    private $userId;

    /**
     * @ORM\Column(type="FileSourceType")
     * @Assert\Choice(choices=FileSourceType::ENUMS)
     */
    private $source = FileSourceType::SOURCE_PUBLIC;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $originalFilename;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero()
     */
    private $fileSize;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fileDate;

    /**
     * @ORM\Column(type="string", length=160)
     * @Assert\Regex(pattern="#\w+/[-+.\w]+#")
     * @Assert\Length(max=160)
     */
    private $fileType;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $remoteKey;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFinal = false;

    /**
     * @ORM\ManyToOne(targetEntity=UserFile::class, inversedBy="backups")
     */
    private $backupOf;

    /**
     * @ORM\OneToMany(targetEntity=UserFile::class, mappedBy="backupOf", cascade={"persist", "remove"})
     */
    private $backups;

    public function __construct()
    {
        $this->backups = new ArrayCollection();
    }

    protected function getDescriptiveProperties(): array
    {
        return array_merge(['descriptor'], parent::getDescriptiveProperties());
    }

    public function getDescriptor(): string
    {
        $translator = self::getTranslator();
        if ($this->getBackupOf()) {
            $format = "[Backup {{ date }}] {{ original }}";
            $dateFormat = $translator ? $translator->trans('locale.datetime') : 'Y-m-d H:i';
            return $this->maybeTranslate($format, [
                '{{ date }}' => $this->getCreatedAt()->format($dateFormat),
                '{{ original }}' => $this->getBackupOf()->getDescriptor(),
            ]);
        }
        switch ($this->source) {
            case FileSourceType::SOURCE_PRIVATE:
                return $this->getOriginalFilename() . '*';
            case FileSourceType::SOURCE_PUBLIC:
                return $this->getOriginalFilename();
            case FileSourceType::SOURCE_EXTERNAL:
            case FileSourceType::SOURCE_GOOGLE_DRIVE:
                return sprintf('%s (%s)',
                    $translator ? $translator->trans("media.mimes.{$this->getFileType()}") : $this->getFileType(),
                    parse_url($this->getPath(), PHP_URL_HOST)
                );
            default:
                throw new \LogicException("Descriptor not implemented for {$this->source}");
        }
    }

    public function getId(): string
    {
        return (string)$this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUserId(): string
    {
        return (string)$this->userId;
    }

    public function getUserInterface(UserLoaderInterface $userLoader): UserInterface
    {
        return $userLoader->loadUserByIdentifier($this->getUserId());
    }

    public function getUser(UserProfileRepository $userLoader): UserProfile
    {
        return $userLoader->findUser($this->getUserId());
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(?string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getFileSize(): int
    {
        return (int)$this->fileSize;
    }

    public function setFileSize(int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getFileSizeKB($kilo = FileSizeConverter::KILO_BASE2): float
    {
        return $this->getFileSize() / $kilo;
    }

    public function getFileSizeMB($kilo = FileSizeConverter::KILO_BASE2): float
    {
        return $this->getFileSize() / $kilo / $kilo;
    }

    public function getFileSizeHuman($kilo = FileSizeConverter::KILO_BASE2): string
    {
        return FileSizeConverter::fileSizeToHuman($this->getFileSize(), $kilo, self::getTranslator());
    }

    public function getFileDate(): \DateTimeInterface
    {
        return $this->fileDate;
    }

    public function setFileDate(\DateTimeInterface $fileDate): self
    {
        $this->fileDate = $fileDate;

        return $this;
    }

    public function getFileType(): string
    {
        return (string)$this->fileType;
    }

    public function setFileType(string $fileType): self
    {
        $this->fileType = $fileType;

        return $this;
    }

    public function getPath(): string
    {
        return (string)$this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getRemoteKey(): ?string
    {
        return $this->remoteKey;
    }

    public function setRemoteKey(?string $remoteKey): self
    {
        $this->remoteKey = $remoteKey;

        return $this;
    }

    private function getTempUpload($source)
    {
        if (array_key_exists($source, $this->tempUploads)) {
            return $this->tempUploads[$source];
        }
        return null;
    }

    private function setTempUpload($source, $value): self
    {
        if (!empty($value)) {
            $this->tempUploads[$source] = $value;
        } else {
            unset($this->tempUploads[$source]);
        }
        return $this;
    }

    public function getPrivateUpload(): ?UploadedFile
    {
        return $this->getTempUpload(FileSourceType::SOURCE_PRIVATE);
    }

    public function getPublicUpload(): ?UploadedFile
    {
        return $this->getTempUpload(FileSourceType::SOURCE_PUBLIC);
    }

    public function getExternalUpload(): ?string
    {
        return $this->getTempUpload(FileSourceType::SOURCE_EXTERNAL);
    }

    public function getGoogleDriveUpload(): ?string
    {
        return $this->getTempUpload(FileSourceType::SOURCE_GOOGLE_DRIVE);
    }

    public function getRemoveUpload(): ?bool
    {
        return $this->getTempUpload('delete');
    }

    public function getSourceUpload(): ?string
    {
        return $this->getTempUpload('source');
    }

    public function setPrivateUpload($value): self
    {
        return $this->setTempUpload(FileSourceType::SOURCE_PRIVATE, $value);
    }

    public function setPublicUpload($value): self
    {
        return $this->setTempUpload(FileSourceType::SOURCE_PUBLIC, $value);
    }

    public function setExternalUpload($value): self
    {
        return $this->setTempUpload(FileSourceType::SOURCE_EXTERNAL, $value);
    }

    public function setGoogleDriveUpload($value): self
    {
        return $this->setTempUpload(FileSourceType::SOURCE_GOOGLE_DRIVE, $value);
    }

    public function setRemoveUpload($value): self
    {
        return $this->setTempUpload('delete', $value);
    }

    public function setSourceUpload($value): self
    {
        return $this->setTempUpload('source', $value);
    }

    public function getIsFinal(): bool
    {
        return $this->isFinal;
    }

    public function setIsFinal(bool $isFinal): self
    {
        $this->isFinal = $isFinal;

        return $this;
    }

    public function maybeBackup(EntityManagerInterface $entityManager): ?UserFile
    {
        // Só cria um backup se o arquivo tiver mudado.
        if ($this->getSourceUpload()) {
            $backup = (new self())
                ->setUserId($this->getUserId())
                ->setSource($this->getSource())
                ->setTitle($this->maybeTranslate("[Backup] {{ title }}", ['{{ title }}' => $this->getTitle()]))
                ->setOriginalFilename($this->getOriginalFilename())
                ->setFileSize($this->getFileSize())
                ->setFileDate($this->getFileDate())
                ->setFileType($this->getFileType())
                ->setPath($this->getPath())
                ->setRemoteKey($this->getRemoteKey())
                ->setIsFinal($this->getIsFinal())
                ->setBackupOf($this)
                ->setId(Uuid::v4());
            $entityManager->persist($backup);
            return $backup;
        }
        return null;
    }

    public function getBackupOf(): ?self
    {
        return $this->backupOf;
    }

    public function setBackupOf(?self $backupOf): self
    {
        $this->backupOf = $backupOf;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getBackups(): Collection
    {
        return $this->backups;
    }

    public function addBackup(self $backup): self
    {
        if (!$this->backups->contains($backup)) {
            $this->backups[] = $backup;
            $backup->setBackupOf($this);
        }

        return $this;
    }

    public function removeBackup(self $backup): self
    {
        if ($this->backups->removeElement($backup)) {
            // set the owning side to null (unless already changed)
            if ($backup->getBackupOf() === $this) {
                $backup->setBackupOf(null);
            }
        }

        return $this;
    }

}
