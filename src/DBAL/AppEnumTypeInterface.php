<?php

namespace Saldanhakun\AppHelper\DBAL;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Contrato para os tipos enumerados da aplicação, baseados em AbstractEnumType.
 * Todos os métodos abaixo são praticamente idênticos para todas as classes, pois
 * trabalham com constantes pré-estabelecidas no nível da classe. Só não funciona
 * como herança de classe por que é tudo feito no nível estático da Classe PHP, e
 * isso implica em certas limitações. Logo, temos que lidar com duplicação de código.
 *
 * Para criar um novo tipo, basta copiar um dos existentes e ajustar as constantes.
 */
interface AppEnumTypeInterface
{

    /**
     * Retorna a chave de tradução para o Nome de um Enum válido
     */
    public static function name($enum): string;

    /**
     * Retorna a chave de tradução para a Descrição de um Enum válido
     */
    public static function description($enum): string;

    /**
     * Lista de Enums com seus Nomes traduzidos
     */
    public static function getEnums(?TranslatorInterface $translator): array;

    /**
     * Lista de Enums com suas Descrições traduzidos
     */
    public static function getTranslatedDescriptions(?TranslatorInterface $translator): array;

    /**
     * Lista de Choices usando os nomes traduzidos. Nomes usados como chaves.
     */
    public static function getTranslatedChoices(?TranslatorInterface $translator): array;
}