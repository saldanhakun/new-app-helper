<?php

namespace Saldanhakun\AppHelper\DBAL;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Estrutura para armazenamento do estado do processo de Validação de uma informação qualquer.
 * @extends AbstractEnumType<string,string>
 */
final class ValidationStatusType extends AbstractEnumType implements AppEnumTypeInterface
{

    public const STATE_PENDING = 'pending';
    public const STATE_INITIATED = 'initiated';
    public const STATE_SUBMITTED = 'submitted';
    public const STATE_CONFIRMED = 'confirmed';
    public const STATE_EXPIRED = 'expired';
    public const STATE_CANCELLED = 'cancelled';
    public const STATE_BLOCKED = 'blocked';

    /**
     * @const string[] Lista de Enums válidos
     */
    public const ENUMS = [
        self::STATE_PENDING,
        self::STATE_INITIATED,
        self::STATE_SUBMITTED,
        self::STATE_CONFIRMED,
        self::STATE_EXPIRED,
        self::STATE_CANCELLED,
        self::STATE_BLOCKED,
    ];

    // Prefixo da chave de tradução, conforme: "<prefixo><enum><radical>"
    protected const TRANS = 'workflow.validation-state.';
    // Radical da chave de tradução para o Nome
    protected const TRANS_NAME = '.name';
    // Radical da chave de tradução para a Descrição
    protected const TRANS_DESCRIPTION = '.description';

    // Vincula os Enums aos Nomes (@see AbstractEnumType)
    protected static $choices = [
        self::STATE_PENDING => self::TRANS . self::STATE_PENDING . self::TRANS_NAME,
        self::STATE_INITIATED => self::TRANS . self::STATE_INITIATED . self::TRANS_NAME,
        self::STATE_SUBMITTED => self::TRANS . self::STATE_SUBMITTED . self::TRANS_NAME,
        self::STATE_CONFIRMED => self::TRANS . self::STATE_CONFIRMED . self::TRANS_NAME,
        self::STATE_EXPIRED => self::TRANS . self::STATE_EXPIRED . self::TRANS_NAME,
        self::STATE_CANCELLED => self::TRANS . self::STATE_CANCELLED . self::TRANS_NAME,
        self::STATE_BLOCKED => self::TRANS . self::STATE_BLOCKED . self::TRANS_NAME,
    ];
    // Vincula os Enums às Descrições
    protected static $descriptions = [
        self::STATE_PENDING => self::TRANS . self::STATE_PENDING . self::TRANS_DESCRIPTION,
        self::STATE_INITIATED => self::TRANS . self::STATE_INITIATED . self::TRANS_DESCRIPTION,
        self::STATE_SUBMITTED => self::TRANS . self::STATE_SUBMITTED . self::TRANS_DESCRIPTION,
        self::STATE_CONFIRMED => self::TRANS . self::STATE_CONFIRMED . self::TRANS_DESCRIPTION,
        self::STATE_EXPIRED => self::TRANS . self::STATE_EXPIRED . self::TRANS_DESCRIPTION,
        self::STATE_CANCELLED => self::TRANS . self::STATE_CANCELLED . self::TRANS_DESCRIPTION,
        self::STATE_BLOCKED => self::TRANS . self::STATE_BLOCKED . self::TRANS_DESCRIPTION,
    ];

    /**
     * Verifica o estado é considerado Final. Útil por exemplo para liberar novas tentativas
     * @param string $state
     * @return bool
     */
    public static function isFinalState(string $state): bool
    {
        return in_array($state, [
            self::STATE_CANCELLED,
            self::STATE_BLOCKED,
            self::STATE_EXPIRED,
            self::STATE_CONFIRMED,
        ], true);
    }

    /**
     * Retorna a chave de tradução para o Nome de um Enum válido
     */
    public static function name($enum): string
    {
        return self::TRANS . $enum . self::TRANS_NAME;
    }

    /**
     * Retorna a chave de tradução para a Descrição de um Enum válido
     */
    public static function description($enum): string
    {
        return self::TRANS . $enum . self::TRANS_DESCRIPTION;
    }

    /**
     * Lista de Enums com seus Nomes traduzidos
     */
    public static function getEnums(?TranslatorInterface $translator): array
    {
        $list = [];
        foreach (self::ENUMS as $enum) {
            $list[$enum] = $translator ? $translator->trans(self::name($enum)) : $enum;
        }
        return $list;
    }

    /**
     * Lista de Enums com suas Descrições traduzidos
     */
    public static function getTranslatedDescriptions(?TranslatorInterface $translator): array
    {
        $list = [];
        foreach (self::ENUMS as $enum) {
            $list[$enum] = $translator ? $translator->trans(self::description($enum)) : '';
        }
        return $list;
    }

    /**
     * Lista de Choices usando os nomes traduzidos. Nomes usados como chaves.
     */
    public static function getTranslatedChoices(?TranslatorInterface $translator): array
    {
        if ($translator) {
            return array_flip(self::getEnums($translator));
        }
        else {
            return self::getChoices();
        }
    }

}