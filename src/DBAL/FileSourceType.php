<?php

namespace Saldanhakun\AppHelper\DBAL;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Estrutura para armazenamento do tipo de origem ou repositório de um arquivo ou mídia
 * @extends AbstractEnumType<string,string>
 */
final class FileSourceType extends AbstractEnumType implements AppEnumTypeInterface
{

    public const SOURCE_PUBLIC = 'public';
    public const SOURCE_PRIVATE = 'private';
    public const SOURCE_EXTERNAL = 'external';
    public const SOURCE_GOOGLE_DRIVE = 'google-drive';

    public const ENUMS = [
        self::SOURCE_PUBLIC,
        self::SOURCE_PRIVATE,
        self::SOURCE_EXTERNAL,
        self::SOURCE_GOOGLE_DRIVE,
    ];

    // Prefixo da chave de tradução, conforme: "<prefixo><enum><radical>"
    protected const TRANS = 'media.source.';
    // Radical da chave de tradução para o Nome
    protected const TRANS_NAME = '.name';
    // Radical da chave de tradução para a Descrição
    protected const TRANS_DESCRIPTION = '.description';

    // Vincula os Enums aos Nomes (@see AbstractEnumType)
    protected static $choices = [
        self::SOURCE_PUBLIC => self::TRANS . self::SOURCE_PUBLIC . self::TRANS_NAME,
        self::SOURCE_PRIVATE => self::TRANS . self::SOURCE_PRIVATE . self::TRANS_NAME,
        self::SOURCE_EXTERNAL => self::TRANS . self::SOURCE_EXTERNAL . self::TRANS_NAME,
        self::SOURCE_GOOGLE_DRIVE => self::TRANS . self::SOURCE_GOOGLE_DRIVE . self::TRANS_NAME,
    ];
    // Vincula os Enums às Descrições
    protected static $descriptions = [
        self::SOURCE_PUBLIC => self::TRANS . self::SOURCE_PUBLIC . self::TRANS_DESCRIPTION,
        self::SOURCE_PRIVATE => self::TRANS . self::SOURCE_PRIVATE . self::TRANS_DESCRIPTION,
        self::SOURCE_EXTERNAL => self::TRANS . self::SOURCE_EXTERNAL . self::TRANS_DESCRIPTION,
        self::SOURCE_GOOGLE_DRIVE => self::TRANS . self::SOURCE_GOOGLE_DRIVE . self::TRANS_DESCRIPTION,
    ];

    public static function isLocal($enum): bool
    {
        return $enum === self::SOURCE_PRIVATE || $enum === self::SOURCE_PUBLIC;
    }

    /**
     * Retorna a chave de tradução para o Nome de um Enum válido
     */
    public static function name($enum): string
    {
        return self::TRANS . $enum . self::TRANS_NAME;
    }

    /**
     * Retorna a chave de tradução para a Descrição de um Enum válido
     */
    public static function description($enum): string
    {
        return self::TRANS . $enum . self::TRANS_DESCRIPTION;
    }

    /**
     * Lista de Enums com seus Nomes traduzidos
     */
    public static function getEnums(?TranslatorInterface $translator): array
    {
        $list = [];
        foreach (self::ENUMS as $enum) {
            $list[$enum] = $translator ? $translator->trans(self::name($enum)) : $enum;
        }
        return $list;
    }

    /**
     * Lista de Enums com suas Descrições traduzidos
     */
    public static function getTranslatedDescriptions(?TranslatorInterface $translator): array
    {
        $list = [];
        foreach (self::ENUMS as $enum) {
            $list[$enum] = $translator ? $translator->trans(self::description($enum)) : '';
        }
        return $list;
    }

    /**
     * Lista de Choices usando os nomes traduzidos. Nomes usados como chaves.
     */
    public static function getTranslatedChoices(?TranslatorInterface $translator): array
    {
        if ($translator) {
            return array_flip(self::getEnums($translator));
        }
        else {
            return self::getChoices();
        }
    }

}