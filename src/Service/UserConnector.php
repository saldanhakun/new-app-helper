<?php

namespace Saldanhakun\AppHelper\Service;

class UserConnector
{

    static private $singleton = null;
    final public static function singleton(): self
    {
        if (!self::$singleton) {
            self::$singleton = new self();
        }
        return self::$singleton;
    }


}