<?php

namespace Saldanhakun\AppHelper\Service;

use Saldanhakun\AppHelper\Event\FileInspectionEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpClient\CurlHttpClient;

class ExternalFileInspector implements EventSubscriberInterface
{

    /** @var FileInspectionResult[] Cache de resultados para maior eficiência. Útil na dinâmica Validação->Tratamento, por exemplo */
    static private $cache = [];
    private $dispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->dispatcher = $eventDispatcher;
    }

    public static function getSubscribedEvents()
    {
        return [
            FileInspectionEvent::NAME => 'execute',
        ];
    }

    public function execute(FileInspectionEvent $event)
    {
        $url = $event->getUrl();
        if (array_key_exists($url, self::$cache)) {
            $event->setResult(self::$cache[$url]);
        }
        else {
            $result = $this->inspect($event);
            self::$cache[$url] = $result;
            $event->setResult($result);
        }
    }

    protected function inspect(FileInspectionEvent $event): FileInspectionResult
    {
        $result = new FileInspectionResult($event->getUrl());
        $client = new CurlHttpClient();
        try {
            $response = $client->request($event->getMethod(), $event->getUrl());
            $statusCode = $response->getStatusCode();
        } catch (\Exception $e) {
            $result->setStatusCode(0)
                ->addError($e->getMessage());
            return $result;
        }

        $headers = array_map(function($value){
            return empty($value) ? null : $value[0];
        }, $response->getHeaders(false));
        $result->setHeaders($headers)->setStatusCode($statusCode);

        if (in_array($statusCode, [404, 410], true)) {
            return $result->setFound(false);
        }
        if (in_array($statusCode, [401, 403], true)) {
            return $result->setFound(true)->setDenied(true);
        }
        if (in_array($statusCode, [301, 302], true)) {
            $result->setFound(true)->setDenied(false)->setRedirected(true);
            if (!$event->getFollowRedirect()) {
                return $result;
            }
            $location = isset($headers['location']) ? $headers['location'] : '';
            $nextEvent = new FileInspectionEvent($location, $event->getMethod(), $event->getRemainingRedirects() - 1);
            $this->dispatcher->dispatch($nextEvent, $nextEvent::NAME);
            return $result->setTarget($nextEvent->getResult());
        }
        if ($statusCode < 200 || $statusCode >= 300) {
            return $result->addError("HTTP: $statusCode");
        }
        $result->setFound(true)->setSuccess(true);

        // Tentativa de identificar o tamanho
        $size = isset($headers['content-length']) ? (int)$headers['content-length'] : 0;
        if ($size) {
            $result->setSize($size);
        }

        // Tentativa de identificar o formato
        $type = isset($headers['content-type']) ? $headers['content-type'] : '';
        if (strpos($type, ';') !== false) {
            $type = trim(explode(';', $type)[0]);
        }
        if (!empty($type)) {
            $result->setType($type);
        }

        // Detecção de Google Drive
        if (preg_match('@^https://(drive|docs).google.com/@i', $event->getUrl())) {
            $result->setIsGoogleDrive(true);
            if (empty($type)) {
                $path = parse_url($event->getUrl(), PHP_URL_PATH);
                if (strpos($path, '/spreadsheets/') === 0) {
                    $result->setType('google-drive/spreadsheet');
                } elseif (strpos($path, '/document/') === 0) {
                    $result->setType('google-drive/document');
                } elseif (strpos($path, '/presentation/') === 0) {
                    $result->setType('google-drive/presentation');
                } elseif (strpos($path, '/file/') === 0) {
                    $result->setType('google-drive/file');
                } elseif (strpos($path, '/drive/folders/') === 0) {
                    $result->setType('google-drive/folder');
                }
            }
        }

        return $result;
    }
}