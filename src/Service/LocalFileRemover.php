<?php

namespace Saldanhakun\AppHelper\Service;

use App\Entity\UserFile;use Doctrine\Persistence\Event\LifecycleEventArgs;use Saldanhakun\AppHelper\DBAL\FileSourceType;use Saldanhakun\AppHelper\Repository\UserProfileRepository;

class LocalFileRemover
{

    private $uploader;
    private $userLoader;

    public function __construct(UserFileUploader $userFileUploader, UserProfileRepository $userLoader)
    {
        $this->uploader = $userFileUploader;
        $this->userLoader = $userLoader;
    }

    public function postRemove(UserFile $userFile, LifecycleEventArgs $event): void
    {
        if ($userFile->getSource() === FileSourceType::SOURCE_PUBLIC) {
            $path = implode(DIRECTORY_SEPARATOR, [
                $this->uploader->getTargetDirectory(false, $userFile->getUser($this->userLoader)),
                $userFile->getPath(),
            ]);
            if (file_exists($path)) {
                @unlink($path);
            }
        }
        if ($userFile->getSource() === FileSourceType::SOURCE_PRIVATE) {
            $path = implode(DIRECTORY_SEPARATOR, [
                $this->uploader->getTargetDirectory(true, $userFile->getUser($this->userLoader)),
                $userFile->getPath(),
            ]);
            if (file_exists($path)) {
                @unlink($path);
            }
        }
    }
}