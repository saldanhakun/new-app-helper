<?php

namespace Saldanhakun\AppHelper\Service;

use Symfony\Contracts\Translation\TranslatorInterface;

class FileSizeConverter
{
    public const KILO_BASE10 = 1000;
    public const KILO_BASE2 = 1024;

    public static function fileSizeToHuman(int $bytes, $kilo, ?TranslatorInterface $translator): string
    {
        $threshold = $kilo === self::KILO_BASE2 ? 1128 : 1200;
        $b = 'B';
        $kb = $kilo === self::KILO_BASE2 ? 'KB' : 'kiB';
        $mb = $kilo === self::KILO_BASE2 ? 'MB' : 'miB';
        $gb = $kilo === self::KILO_BASE2 ? 'GB' : 'giB';
        $dot = '.';
        if ($translator) {
            $b = $translator->trans("locale.filesize.$b");
            $kb = $translator->trans("locale.filesize.$kb");
            $mb = $translator->trans("locale.filesize.$mb");
            $gb = $translator->trans("locale.filesize.$gb");
            $dot = $translator->trans("locale.filesize.dot");
        }
        $kbytes = $bytes / $kilo;
        $mbytes = $kbytes / $kilo;
        $gbytes = $mbytes / $kilo;
        if ($bytes <= $threshold) {
            return str_replace('.', $dot, sprintf('%d %s', $bytes, $b));
        }
        elseif ($kbytes < $threshold) {
            return str_replace('.', $dot, sprintf('%0.1f %s', $kbytes, $kb));
        }
        elseif ($mbytes < $threshold) {
            return str_replace('.', $dot, sprintf('%0.1f %s', $mbytes, $mb));
        }
        else {
            return str_replace('.', $dot, sprintf('%0.1f %s', $gbytes, $gb));
        }
    }

    public static function fileSizeToHumanAlt(int $bytes): string
    {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        $dec = $factor ? 2 : 0;

        return sprintf("%.{$dec}f", $bytes / (1024 ** $factor)) . @$size[$factor];
    }

    public static function fileSizeFromHuman(string $humanSize, ?TranslatorInterface $translator): int
    {
        if (!preg_match('/^([\.\,0-9]+)[ ]?([a-z]+)$/i', $humanSize, $matches)) {
            throw new \InvalidArgumentException("Not a human file size: $humanSize");
        }
        $b = 'B';
        $kb = 'KB';
        $mb = 'MB';
        $gb = 'GB';
        $kib = 'kiB';
        $mib = 'miB';
        $gib = 'giB';
        $dot = '.';
        if ($translator) {
            $b = $translator->trans("locale.filesize.$b");
            $kb = $translator->trans("locale.filesize.$kb");
            $mb = $translator->trans("locale.filesize.$mb");
            $gb = $translator->trans("locale.filesize.$gb");
            $kib = $translator->trans("locale.filesize.$kib");
            $mib = $translator->trans("locale.filesize.$mib");
            $gib = $translator->trans("locale.filesize.$gib");
            $dot = $translator->trans("locale.filesize.dot");
        }
        foreach (explode(',', "b,kb,kib,mb,mib,gb,gib") as $var) {
            $$var = strtolower($$var);
        }

        $amount = (float)str_replace($dot, '.', $matches[1]);
        switch (strtolower($matches[2])) {
            case $b: return (int) $amount;
            case $kb: return (int) ($amount * self::KILO_BASE2);
            case $kib: return (int) ($amount * self::KILO_BASE10);
            case $mb: return (int) ($amount * self::KILO_BASE2 * self::KILO_BASE2);
            case $mib: return (int) ($amount * self::KILO_BASE10 * self::KILO_BASE10);
            case $gb: return (int) ($amount * self::KILO_BASE2 * self::KILO_BASE2 * self::KILO_BASE2);
            case $gib: return (int) ($amount * self::KILO_BASE10 * self::KILO_BASE10 * self::KILO_BASE10);
            default: throw new \InvalidArgumentException("Scale unknown: {$matches[2]}");
        }
    }

}