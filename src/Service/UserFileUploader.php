<?php

namespace Saldanhakun\AppHelper\Service;

use App\Entity\UserFile;
use Doctrine\ORM\EntityManagerInterface;
use Saldanhakun\AppHelper\DBAL\FileSourceType;
use Saldanhakun\AppHelper\Event\FileInspectionEvent;
use Saldanhakun\AppHelper\Repository\UserProfileRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Uid\Uuid;

class UserFileUploader
{
    private $privateRepository;
    private $publicRepository;
    private $slugger;
    private $encryptedFilenames = false;
    private $entityManager;
    private $eventDispatcher;
    private $userLoader;

    public function __construct($privateRepository, $publicRepository, SluggerInterface $slugger, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, UserProfileRepository $userLoader)
    {
        $this->privateRepository = rtrim($privateRepository, DIRECTORY_SEPARATOR);
        $this->publicRepository = rtrim($publicRepository, DIRECTORY_SEPARATOR);
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->userLoader = $userLoader;
    }

    public function getEncryptedFilenames(): bool
    {
        return $this->encryptedFilenames;
    }

    public function setEncryptedFilenames(bool $value): self
    {
        $this->encryptedFilenames = $value;
        return $this;
    }

    /**
     * Gera um nome único de arquivo para armazenagem literal no sistema de arquivos.
     * Tenta manter a extensão original do arquivo.
     * O basename pode ser uma chave criptografada ou uma versão segura do nome do arquivo original.
     * @param UploadedFile $file
     * @return string
     */
    protected function generateFilename(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        if ($this->encryptedFilenames) {
            $fileName = md5($safeFilename . Uuid::v4());
        }
        else {
            $fileName = $safeFilename . '-' . uniqid('', true);
        }
        return $fileName . '.' . $file->guessExtension();
    }

    /**
     * Retorna a pasta-base para o repositório local no nível de visibilidade solicitado
     * @param bool $private
     * @return string
     */
    protected function getLocalRepositoryPath(bool $private): string
    {
        $path = $private ? $this->privateRepository : $this->publicRepository;
        if (!is_dir($path) && !mkdir($path) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }
        return $path;
    }

    /**
     * Retorna a pasta-base para arquivos do usuário indicado, conforme visibilidade.
     * Usa o UUID no repositório privado, e um hash MD5 dele no público. Em ambos, os arquivos de cada usuário
     * estão separados dos demais.
     * @param bool $private
     * @param UserInterface $user
     * @return string
     */
    public function getTargetDirectory(bool $private, UserInterface $user): string
    {
        $path = implode(DIRECTORY_SEPARATOR, [
            $this->getLocalRepositoryPath($private),
            $private ? $user->getUserIdentifier() : md5($user->getUserIdentifier()),
        ]);
        if (!is_dir($path) && !mkdir($path) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }
        return $path;
    }

    /**
     * Efetiva o upload, retornando o nome local do arquivo. A pasta não é armazenada no banco, mas pode ser
     * reconstituída com base no ID do usuário
     * @param UploadedFile $file
     * @param bool $private
     * @param UserInterface $user
     * @return UserFile
     */
    public function handleDirectLocalUpload(UploadedFile $file, bool $private, UserInterface $user): UserFile
    {
        $fileName = $this->generateFilename($file);
        $path = $this->getTargetDirectory($private, $user);

        try {
            $file->move($path, $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            throw $e;
        }

        $userFile = (new UserFile())
            ->setId(Uuid::v4())
            ->setUserId($user->getUserIdentifier())
            ->setPath($fileName)
            ->setOriginalFilename($file->getClientOriginalName())
            ->setSource($private ? FileSourceType::SOURCE_PRIVATE : FileSourceType::SOURCE_PUBLIC);
        $this->fillLocalFileProperties($userFile);

        return $userFile;
    }

    protected function fillLocalFileProperties(UserFile $media): self
    {
        $path = implode(DIRECTORY_SEPARATOR, [
            $this->getTargetDirectory($media->getSource() === FileSourceType::SOURCE_PRIVATE, $media->getUser($this->userLoader)),
            $media->getPath(),
        ]);

        if (is_file($path)) {
            $date = (new \DateTime())->setTimestamp(filectime($path));
            $size = filesize($path);
            $type = mime_content_type($path);
        } else {
            $date = new \DateTime();
            $size = 0;
            $type = 'unknown/type';
        }
        $media
            ->setFileDate($date)
            ->setFileType($type)
            ->setFileSize($size);
        return $this;
    }

    /**
     * Processa a submissão de arquivos por múltiplas fontes. Assume que um formulário contendo um campo do tipo
     * UserFileType (ou o próprio UserFileType) foi processado e validado.
     * O arquivo pode estar em 3 contextos:
     * 1. não existir (null) significando um arquivo não obrigatório e que não foi submetido
     * 2. objeto novo (sem id) significando que um novo upload foi realizado
     * 3. objeto existente (com id) que pode ou não ter sido modificado de alguma forma
     * @param UserFile|null $media
     * @param UserInterface $user
     * @return UserFile|null
     */
    public function handleUpload(?UserFile $media, UserInterface $user): ?UserFile
    {
        if ($media === null || (!$media->getId() && !$media->getSourceUpload())) {
            // Efetivamente, um upload vazio.
            return null;
        }
        if ($media->getId()) {
            if ($media->getIsFinal()) {
                // Não permite nenhuma alteração
                return $media;
            }
            // Permite um backup de segurança
            $media->maybeBackup($this->entityManager);
            if ($media->getRemoveUpload()) {
                // Exclui o arquivo, e sinaliza o desvínculo da origem.
                $this->entityManager->remove($media);
                return null;
            }
            if (!$media->getSourceUpload()) {
                // Arquivo existente sem modificação
                return $media;
            }
        }
        else {
            $media->setUserId($user->getUserIdentifier());
        }

        $source = $media->getSourceUpload();
        if ($source === FileSourceType::SOURCE_PUBLIC || $source === FileSourceType::SOURCE_PRIVATE) {
            if ($source === FileSourceType::SOURCE_PRIVATE) {
                $file = $media->getPrivateUpload();
                //$this->setEncryptedFilenames(true);
            }
            else {
                $file = $media->getPublicUpload();
                //$this->setEncryptedFilenames(false);
            }
            if ($file === null) {
                throw new \LogicException("Upload file unavailable");
            }
            $fileName = $this->generateFilename($file);
            $path = $this->getTargetDirectory($media->getSourceUpload() === FileSourceType::SOURCE_PRIVATE, $user);

            try {
                $file->move($path, $fileName);
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                throw $e;
            }

            $media
                ->setPath($fileName)
                ->setOriginalFilename($file->getClientOriginalName());
            $this->fillLocalFileProperties($media);
        }
        elseif ($source === FileSourceType::SOURCE_EXTERNAL || $source === FileSourceType::SOURCE_GOOGLE_DRIVE) {
            // A URL já passou pelas validações, onde checou-se o StatusCode e até tipo e tamanho do conteúdo.
            // Aqui podemos recuperar a inspeção de URL realizada (no cache) para pegar dados úteis do arquivo.
            if ($source === FileSourceType::SOURCE_EXTERNAL) {
                $event = new FileInspectionEvent($media->getExternalUpload());
            }
            else {
                $event = new FileInspectionEvent($media->getGoogleDriveUpload());
            }
            $this->eventDispatcher->dispatch($event, $event::NAME);
            $result = $event->getResult();
            $media
                ->setPath($result->getUrl())
                ->setFileSize($result->getSize())
                ->setFileType($result->getType())
                ->setFileDate($result->getMoment()) // Talvez a data do recurso?
                ->setOriginalFilename($result->getLeafName());
            if ($source === FileSourceType::SOURCE_GOOGLE_DRIVE) {
                if (preg_match('@/d/([^/]+)/@i', $result->getUrl(), $match)) {
                    $media->setRemoteKey($match[1]);
                }
            }
        }

        $media
            ->setUserId($user->getUserIdentifier())
            ->setSource($source);
        if (empty($media->getId())) {
            $media->setId(Uuid::v4());
        }
        $this->entityManager->persist($media);
        return $media;
    }
}