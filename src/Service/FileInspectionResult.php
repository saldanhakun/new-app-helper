<?php

namespace Saldanhakun\AppHelper\Service;

/**
 * O resultado de uma inspeção de arquivo remoto
 */
class FileInspectionResult
{

    private $url;
    private $moment;
    private $errors = [];
    private $statusCode = 0;
    private $found = false;
    private $denied = false;
    private $redirected = false;
    private $target;
    private $success = false;
    private $headers = [];

    private $size = 0;
    private $type;
    private $isGoogleDrive = false;

    public function __construct($url)
    {
        $this->url = $url;
        $this->moment = new \DateTime();
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMoment(): \DateTimeInterface
    {
        return $this->moment;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError($message): self
    {
        $this->errors[] = $message;
        return $this;
    }

    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    public function setStatusCode(?int $statusCode): self
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getFound(): bool
    {
        return $this->found;
    }

    public function setFound(bool $found): self
    {
        $this->found = $found;
        return $this;
    }

    public function getDenied(): bool
    {
        return $this->denied;
    }

    public function setDenied(bool $denied): self
    {
        $this->denied = $denied;
        return $this;
    }

    public function getRedirected(): bool
    {
        return $this->redirected;
    }

    public function setRedirected(bool $redirected): self
    {
        $this->redirected = $redirected;
        return $this;
    }

    public function getTarget(): ?FileInspectionResult
    {
        return $this->target;
    }

    public function setTarget(?FileInspectionResult $target): self
    {
        $this->target = $target;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;
        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;
        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getIsGoogleDrive(): bool
    {
        return $this->isGoogleDrive;
    }

    public function setIsGoogleDrive(bool $isGoogleDrive): self
    {
        $this->isGoogleDrive = $isGoogleDrive;
        return $this;
    }

    public function getLeafName(): string
    {
        $path = explode('/', parse_url($this->url, PHP_URL_PATH));
        return $path[count($path)-1];
    }

}