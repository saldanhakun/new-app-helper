<?php

namespace Saldanhakun\AppHelper\Command;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Saldanhakun\BrazilianValidators\Constraint\Cpf;
use Saldanhakun\BrazilianValidators\Constraint\Phone;
use Saldanhakun\BrazilianValidators\Validator\CpfValidator;
use Saldanhakun\BrazilianValidators\Validator\PhoneValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;
use Symfony\Component\Validator\Constraints\Uuid;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use function Symfony\Component\String\u;

abstract class AppCommand extends Command
{
    protected $entityManager;
    protected $eventDispatcher;
    protected $translator;

    /**
     * Retorna a lista de restrições aplicáveis a uma nova senha
     * @param bool $required
     * @return Constraint[]
     */
    public static function passwordConstraints(bool $required=true): array
    {
        return array_filter([
            $required ? new NotBlank() : null,
            new Length(['min' => 8, 'max' => 30]),
            new NotCompromisedPassword(),
        ]);
    }

    protected function getUserRepository(): ?ServiceEntityRepositoryInterface
    {
        throw new \LogicException("Acesso ao repositório de usuários não configurado na aplicação para AppCommand.");
    }

    public function __construct(string $name = null, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, TranslatorInterface $translator)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->translator = $translator;
    }

    protected function throwViolations(ConstraintViolationListInterface $violations)
    {
        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                /** @var ConstraintViolationInterface $violation */
                $errors[] = $violation->getMessage();
            }
            throw new \RuntimeException(implode("\n", $errors));
        }
    }

    protected function trans($message, array $args=[], $domain=null): string
    {
        return $this->translator->trans($message, $args, $domain);
    }
    protected function transArray(array $array, array $args=[], $domain=null): array
    {
        $trans = [];
        foreach ($array as $key => $value) {
            $trans[$key] = $this->translator->trans($value, $args, $domain);
        }
        return $trans;
    }

    protected function askForEmail(SymfonyStyle $io, string $title, bool $required, ?bool $matchUser, ?string $default): string
    {
        $question = (new Question($this->trans($title), $default))
            ->setValidator(function ($answer) use ($required, $matchUser) {
                if ($required && empty(trim($answer))) {
                    throw new \RuntimeException($this->trans("You must inform an e-mail address", [], 'validators'));
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Email(),
                    new Callback(function($answer, ExecutionContextInterface $context) use ($matchUser) {
                        if ($matchUser !== null) {
                            $user = $this->getUserRepository()->findOneBy(['email' => $answer]);
                            if ($matchUser && !$user) {
                                $context->buildViolation("E-mail not found")
                                    ->setTranslationDomain('validators')
                                    ->addViolation();
                            }
                            if (!$matchUser && $user) {
                                $context->buildViolation("E-mail already in use")
                                    ->setTranslationDomain('validators')
                                    ->addViolation();
                            }
                        }
                    }),
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setNormalizer(function($answer){
                return u($answer)->lower()->trim();
            })
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForUuid(SymfonyStyle $io, string $title): string
    {
        $question = (new Question($this->trans($title), null))
            ->setValidator(function ($answer) {
                if (empty(trim($answer))) {
                    throw new \RuntimeException($this->trans("You must inform an user UUID", [], 'validators'));
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Uuid(),
                    new Callback(function($answer, ExecutionContextInterface $context){
                        $user = $this->getUserRepository()->findOneBy(['id' => $answer]);
                        if (!$user) {
                            $context->buildViolation("User not found")
                                ->setTranslationDomain('validators')
                                ->addViolation();
                        }
                    }),
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setNormalizer(function($answer){
                return u($answer)->lower()->trim();
            })
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForName(SymfonyStyle $io, string $title, bool $required, ?string $default, int $minLength, int $maxLength): string
    {
        $question = (new Question($this->trans($title), $default))
            ->setValidator(function ($answer) use ($required, $minLength, $maxLength) {
                if ($required && empty(trim($answer))) {
                    throw new \RuntimeException($this->trans("You must inform an name", [], 'validators'));
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Length([
                        'max' => $maxLength,
                        'min' => $minLength,
                    ]),
                    //new Regex(['pattern' => $fullName ? '/[.]+ [.]+/' : '/./']), // ao menos 1 espaço no nome completo
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setNormalizer(function($answer){
                return u($answer)->trim()->title();
            })
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForPassword(SymfonyStyle $io, string $title, bool $required, ?string $default): string
    {
        $question = (new Question($this->trans($title), $default))
            ->setValidator(function ($answer) use ($default, $required) {
                if (empty(trim($answer))) {
                    if ($default) {
                        $answer = $default;
                    }
                    elseif ($required) {
                        throw new \RuntimeException($this->trans("You must inform an password", [], 'validators'));
                    }
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, self::passwordConstraints());
                $this->throwViolations($violations);
                return $answer;
            })
            ->setHidden(true)
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForDocument(SymfonyStyle $io, string $title, bool $required, ?bool $matchUser, ?string $default): string
    {
        $question = (new Question($this->trans($title), $default))
            ->setValidator(function ($answer) use ($required, $matchUser) {
                if ($required && empty(trim($answer))) {
                    throw new \RuntimeException($this->trans("You must inform a document", [], 'validators'));
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Cpf(),
                    new Callback(function($answer, ExecutionContextInterface $context) use ($matchUser) {
                        if ($matchUser !== null) {
                            $user = $this->getUserRepository()->findOneBy(['document' => $answer]);
                            if ($matchUser && !$user) {
                                $context->buildViolation("User not found")
                                    ->setTranslationDomain('validators')
                                    ->addViolation();
                            }
                            if (!$matchUser && $user) {
                                $context->buildViolation("Document already in use")
                                    ->setTranslationDomain('validators')
                                    ->addViolation();
                            }
                        }
                    }),
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setNormalizer(function($answer){
                return CpfValidator::normalize($answer);
            })
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForPhone(SymfonyStyle $io, string $title, bool $required, ?string $default): string
    {
        $question = (new Question($this->trans($title), $default))
            ->setValidator(function ($answer) use ($required) {
                if ($required && empty(trim($answer))) {
                    throw new \RuntimeException($this->trans("You must inform an mobile phone", [], 'validators'));
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Phone(),
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setNormalizer(function($answer){
                return PhoneValidator::normalize($answer);
            })
            ->setMaxAttempts(3);
        return $io->askQuestion($question);
    }

    protected function askForOption(SymfonyStyle $io, string $title, array $choices, ?string $default): string
    {
        $mappedChoices = array_keys($choices);
        $indexedChoices = array_values($choices);
        $indexedChoices = array_combine(array_map(function($val){ return sprintf('%d', $val + 1); }, array_keys($indexedChoices)), array_values($indexedChoices));
        $defaultIndex = null;
        if ($default !== null && array_key_exists($default, $choices)) {
            $defaultIndex = sprintf('%d', (int) array_search($default, $mappedChoices, false) + 1);
        }
        $question = (new ChoiceQuestion($this->trans($title), $indexedChoices, $defaultIndex))
            ->setValidator(function ($answer) use ($indexedChoices, $defaultIndex) {
                if (empty(trim($answer))) {
                    if ($defaultIndex !== null) {
                        $answer = $defaultIndex;
                    }
                    else {
                        throw new \RuntimeException($this->trans("You must select an option from the list", [], 'validators'));
                    }
                }
                $validator = Validation::createValidator();
                $violations = $validator->validate($answer, [
                    new NotBlank(),
                    new Callback(['callback' => function($answer, ExecutionContextInterface $context) use ($indexedChoices) {
                        if ($answer && !array_key_exists($answer, $indexedChoices)) {
                            $context->buildViolation("The value you selected is not a valid choice.")
                                ->setTranslationDomain('validators')
                                ->addViolation();
                        }
                    }]),
                ]);
                $this->throwViolations($violations);
                return $answer;
            })
            ->setMaxAttempts(3);
        return $mappedChoices[((int)$io->askQuestion($question))-1];
    }

}
