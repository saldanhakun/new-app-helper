<?php

namespace Saldanhakun\AppHelper\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Symfony\Component\String\u;

class DisplayType extends AbstractType
{

    public const WRAP_INPUT = 'input';
    public const WRAP_TEXTAREA = 'textarea';
    public const WRAP_FORM_CONTROL = 'form-control';
    public const WRAP_NONE = '';

    public function getParent()
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'display_property' => null,
            'display_method' => null,
            'callback' => null,
            'with_input' => false,
            'template' => null,
            'wrapper' => self::WRAP_FORM_CONTROL,
            'render_html' => false,
        ]);
        parent::configureOptions($resolver);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $value = $view->vars['value'];
        $view->vars['native_value'] = $value;
        $parentData = null;
        if ($view->parent) {
            $parentData = $view->parent->vars['data'];
        }
        $possibleEntity = is_object($value) ? $value : (is_object($parentData) ? $parentData : null);

        if ($possibleEntity) {
            if (!empty($options['display_method'])) {
                $value = $possibleEntity->{$options['display_method']}();
            }
            elseif (!empty($options['display_property'])) {
                $getter = 'get'.ucfirst(u($options['display_property'])->camel());
                $value = $possibleEntity->$getter();
            }
        }
        if (is_callable($options['callback'])) {
            $value = call_user_func($options['callback'], $value, $parentData);
        }
        $view->vars['value'] = $value;
        $view->vars['twig_template'] = $options['template'];
        $view->vars['wrapper'] = $options['wrapper'];
        $view->vars['with_input'] = $options['with_input'];
        $view->vars['render_html'] = $options['render_html'];
        parent::buildView($view, $form, $options);
    }

    public function getBlockPrefix()
    {
        return 'custom_display';
    }
}