<?php

namespace Saldanhakun\AppHelper\Form;

use App\Entity\UserFile;use Saldanhakun\AppHelper\DBAL\FileSourceType;use Saldanhakun\AppHelper\Event\FileInspectionEvent;use Saldanhakun\AppHelper\Service\FileInspectionResult;use Saldanhakun\AppHelper\Service\FileSizeConverter;use Symfony\Component\EventDispatcher\EventDispatcherInterface;use Symfony\Component\Form\AbstractType;use Symfony\Component\Form\Extension\Core\Type\CheckboxType;use Symfony\Component\Form\Extension\Core\Type\FileType;use Symfony\Component\Form\Extension\Core\Type\HiddenType;use Symfony\Component\Form\Extension\Core\Type\UrlType;use Symfony\Component\Form\FormBuilderInterface;use Symfony\Component\Form\FormEvent;use Symfony\Component\Form\FormEvents;use Symfony\Component\Form\FormInterface;use Symfony\Component\Form\FormView;use Symfony\Component\OptionsResolver\OptionsResolver;use Symfony\Component\Validator\Constraints\Callback;use Symfony\Component\Validator\Constraints\Choice;use Symfony\Component\Validator\Constraints\File;use Symfony\Component\Validator\Constraints\Length;use Symfony\Component\Validator\Constraints\Url;use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UserFileType extends AbstractType
{

    private $dispatcher;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {
                /** @var UserFile $file */
                $file = $event->getData();
                $form = $event->getForm();
                if (!$file || !$options['disabled'] || (!$file->getBackupOf() && !$file->getIsFinal())) {
                    if (in_array(FileSourceType::SOURCE_PRIVATE, $options['sources'], true)) {
                        $form
                            ->add('privateUpload', FileType::class, [
                                'label' => "media.source.private.inputLabel",
                                'required' => $options['required'],
                                'constraints' => [
                                    new File([
                                        'maxSize' => $options['max_size'],
                                        'mimeTypes' => $options['mime_types'],
                                    ]),
                                ],
                            ]);
                    }
                    if (in_array(FileSourceType::SOURCE_PUBLIC, $options['sources'], true)) {
                        $form
                            ->add('publicUpload', FileType::class, [
                                'label' => "media.source.public.inputLabel",
                                'required' => $options['required'],
                                'constraints' => [
                                    new File([
                                        'maxSize' => $options['max_size'],
                                        'mimeTypes' => $options['mime_types'],
                                    ]),
                                ],
                            ]);
                    }
                    if (in_array(FileSourceType::SOURCE_EXTERNAL, $options['sources'], true)) {
                        $form
                            ->add('externalUpload', UrlType::class, [
                                'label' => "media.source.external.inputLabel",
                                'required' => $options['required'],
                                'constraints' => [
                                    new Length(null, null, 255),
                                    new Url(),
                                    new Callback(function($value, ExecutionContextInterface $context) use ($options, $file) {
                                        if (empty($value)) {
                                            return;
                                        }
                                        $event = new FileInspectionEvent($value);
                                        $this->dispatcher->dispatch($event, $event::NAME);
                                        $result = $event->getResult();
                                        $this->commonUrlChecks($result, $context, false);
                                        if ($result->getSuccess()) {
                                            if ($options['max_size'] && $result->getSize()) {
                                                $maxSize = FileSizeConverter::fileSizeFromHuman($options['max_size'], $file::getTranslator());
                                                if ($result->getSize() > $maxSize) {
                                                    $context
                                                        ->buildViolation('File is too large', [
                                                            '{{ size }}' => FileSizeConverter::fileSizeToHuman($result->getSize(), FileSizeConverter::KILO_BASE2, $file::getTranslator()),
                                                            '{{ max }}' => $options['max_size'],
                                                        ])
                                                        ->addViolation();
                                                }
                                            }
                                            if (!empty($options['mime_types']) && $result->getType()) {
                                                if (!in_array($result->getType(), $options['mime_types'], true)) {
                                                    $context
                                                        ->buildViolation('File type is not allowed', [
                                                            '{{ type }}' => $result->getType(),
                                                            '{{ allowed }}' => implode(', ', $options['mime_types']),
                                                        ])
                                                        ->addViolation();
                                                }
                                            }
                                        }
                                    }),
                                ],
                            ]);
                    }
                    if (in_array(FileSourceType::SOURCE_GOOGLE_DRIVE, $options['sources'], true)) {
                        $form
                            ->add('googleDriveUpload', UrlType::class, [
                                'label' => "media.source.google-drive.inputLabel",
                                'required' => $options['required'],
                                'constraints' => [
                                    new Length(null, null, 255),
                                    new Url(),
                                    new Callback(function($value, ExecutionContextInterface $context) use ($options) {
                                        if (empty($value)) {
                                            return;
                                        }
                                        $event = new FileInspectionEvent($value, 'GET', 1);
                                        $this->dispatcher->dispatch($event, $event::NAME);
                                        $result = $event->getResult();
                                        $this->commonUrlChecks($result, $context, true);
                                        if ($result->getSuccess()) {
                                            if (!$result->getIsGoogleDrive()) {
                                                $context
                                                    ->buildViolation('URL is not from Google Drive')
                                                    ->addViolation();
                                                return;
                                            }
                                            if ($options['drive_check_mime'] && !empty($options['mime_types']) && $result->getType()) {
                                                if (!in_array($result->getType(), $options['mime_types'], true)) {
                                                    $context
                                                        ->buildViolation('File type is not allowed', [
                                                            '{{ type }}' => $result->getType(),
                                                            '{{ allowed }}' => implode(', ', $options['mime_types']),
                                                        ])
                                                        ->addViolation();
                                                }
                                            }
                                        }
                                    }),
                                ],
                            ]);
                    }
                    if ($options['ask_title']) {
                        $form->add('title', null, [
                            'label' => 'media.titleLabel',
                        ]);
                    }
                }
                if ($file && !$options['disabled'] && !$options['required'] && !$file->getIsFinal() && !$file->getBackupOf()) {
                    $form->add('removeUpload', CheckboxType::class, [
                        'label' => 'media.removeButton',
                    ]);
                }

                $form->add('sourceUpload', HiddenType::class, [
                    'constraints' => [
                        new Choice($options['sources']),
                    ],
                    'attr' => [
                        'class' => 'source',
                    ],
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserFile::class,
            'user' => null,
            'max_size' => 0,
            'mime_types' => [],
            'drive_check_mime' => false,
            'sources' => FileSourceType::ENUMS,
            'default_source' => null,
            'ask_title' => false,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['sources'] = $options['sources'];
        $view->vars['ask_title'] = $options['ask_title'];
        $defaultSource = $options['default_source'];
        if (empty($defaultSource) || !in_array($defaultSource, $options['sources'], true)) {
            foreach ($options['sources'] as $source) {
                $defaultSource = $source;
                break;
            }
        }
        $view->vars['default_source'] = $defaultSource;
        parent::buildView($view, $form, $options);
    }

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->dispatcher = $eventDispatcher;
    }

    protected function commonUrlChecks(?FileInspectionResult $result, ExecutionContextInterface $context, $allowRedirect)
    {
        if (!$result) {
            $context->buildViolation('External URL validation failed')
                ->addViolation();
            return;
        }
        if (!empty($result->getErrors())) {
            $context->buildViolation(implode("<br>", $result->getErrors()))
                ->addViolation();
            return;
        }
        if ($result->getDenied() && !$result->getIsGoogleDrive()) {
            $context->buildViolation('Access denied on external URL')
                ->addViolation();
            return;
        }
        if (!$result->getFound() && !$result->getDenied()) {
            $context->buildViolation('No file found on external URL')
                ->addViolation();
            return;
        }
        if ($result->getRedirected()) {
            if (!$allowRedirect) {
                $context->buildViolation('Redirection detected. Please privide a final URL')
                    ->addViolation();
            }
            return;
        }
        if (!$result->getSuccess()) {
            $context->buildViolation('Error fetching the URL')
                ->addViolation();
            return;
        }
    }
}