<?php

namespace Saldanhakun\AppHelper\Notification;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Header\Headers;
use Symfony\Component\Mime\Part\AbstractPart;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AppEmail extends TemplatedEmail
{

    /** @var TranslatorInterface */
    protected $translator;

    /**
     * @return Address O remetente padrão da mensagem. Pode ser substituído mais tarde
     */
    protected function getDefaultFrom(): Address
    {
        return new Address('noreply@domain.com', 'Leave me alone');
    }

    /**
     * Prepara a mensagem conforme sua natureza
     */
    protected function configure(): void
    {
        $this
            ->from($this->getDefaultFrom());
    }

    protected function validate(): void
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($this, [
            new Valid(),
        ]);
        $this->throwViolations($violations);
    }

    protected function throwViolations(ConstraintViolationListInterface $violations)
    {
        if ($violations->count()) {
            $errors = [];
            foreach ($violations as $violation) {
                /** @var ConstraintViolationInterface $violation */
                $errors[] = $violation->getMessage();
            }
            throw new \RuntimeException(implode("\n", $errors));
        }
    }

    /**
     * Configura as propriedades da mensagem de acordo com o contexto
     */
    abstract protected function doPrepare(): void;

    public function prepare(?TranslatorInterface $translator=null): self
    {
        $this->translator = $translator;
        $this->validate();
        $this->doPrepare();
        return $this;
    }

    public function __construct(Headers $headers = null, AbstractPart $body = null)
    {
        parent::__construct($headers, $body);
        $this->configure();
    }
}