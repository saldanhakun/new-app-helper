<?php

namespace Saldanhakun\AppHelper\EasyAdmin;

use Saldanhakun\AppHelper\Form\DisplayType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class DisplayField implements FieldInterface
{

    use FieldTrait;

    public const WRAP_INPUT = DisplayType::WRAP_INPUT;
    public const WRAP_TEXTAREA = DisplayType::WRAP_TEXTAREA;
    public const WRAP_FORM_CONTROL = DisplayType::WRAP_FORM_CONTROL;
    public const WRAP_NONE = DisplayType::WRAP_NONE;

    /**
     * @param string|false|null $label
     */
    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplateName('crud/field/generic')
            ->setFormType(DisplayType::class)
            ->addCssClass('field-display')
            ->setDisabled(true)
            ->setDefaultColumns('col-md-9 col-xxl-7');
    }

    public function withInput($withInput=true): self
    {
        return $this->setFormTypeOption('with_input', $withInput);
    }

    public function setDisplayProperty($name): self
    {
        return $this->setFormTypeOption('display_property', $name);
    }

    public function setDisplayMethod($name): self
    {
        return $this->setFormTypeOption('display_method', $name);
    }

    public function setDisplayWrapper($wrapper): self
    {
        return $this->setFormTypeOption('wrapper', $wrapper);
    }

    public function setDisplayAsHtml($html=true): self
    {
        return $this->setFormTypeOption('render_html', $html);
    }

    /**
     * A função de callback que irá definir o valor de exibição do campo.
     * Recebe dois parâmetros: o valor nativo do campo, e o valor do pai do campo.
     * Para Entity Forms, isso implica em geral no valor da propriedade e a Entidade, respectivamente.
     * @param \Closure|array|null $callback
     * @return $this
     */
    public function setDisplayCallback($callback): self
    {
        return $this->setFormTypeOption('callback', $callback);
    }

    /**
     * Formata a massa de dados como uma lista de marcadores ou numerada.
     * Os dados podem ser um arranjo de strings, ou um agrupamento de 2 valores representando rótulo e valor.
     * O rótulo, caso usado, fica em negrito.
     * Ex: ["laranja", "abacaxi"] ==> <ul><li>laranja</li><li>abacaxi</li></ul>
     * Ex: [["nome", "laranja"], ['rotulo'=>"fruta",'valor'=>"abacaxi"]] ==> <ul><li><strong>nome:</strong> laranja</li><li><strong>fruta:</strong> abacaxi</li></ul>
     * @param string[]|array $data
     * @param bool $orderedList
     * @return string
     */
    public static function arrayToList(array $data, bool $orderedList = true): string
    {
        $list = [];
        foreach ($data as $item) {
            if (is_string($item)) {
                $list[] = sprintf('<li>%s</li>', $item);
            }
            else {
                $value = array_pop($item);
                $key = array_pop($item);
                $list[] = sprintf('<li><strong>%s:</strong> %s</li>', $key, $value);
            }
        }

        $tag = $orderedList ? 'ol' : 'ul';
        return sprintf("<%s>%s</%s>", $tag, implode("\n", $list), $tag);
    }

    /**
     * Formata a massa de dados como uma Lista de Definição HTML. O formato pode ser um simples array associativo
     * com o Rótulo como índice, e o valor como valor mesmo. Ou pode ser um array genérico com 2 valores, o
     * primeiro representando o rótulo e o segundo o valor.
     * Ex: ["laranja", "abacaxi"] ==> <dl><dt>0</dt><dd>laranja</dd><dt>1</dt><dd>abacaxi</dd></dl>
     * Ex: [["nome", "laranja"], ['rotulo'=>"fruta",'valor'=>"abacaxi"]] ==> <dl><dt>nome</dt><dd>laranja</dd><dt>fruta</dt><dd>abacaxi</dd></dl>
     * @param array $data
     * @return string
     */
    public static function arrayToDL(array $data): string
    {
        $list = [];
        foreach ($data as $index => $item) {
            if (is_string($item)) {
                $list[] = sprintf('<dt>%s</dt><dd>%s</dd>', $index, $item);
            }
            else {
                $value = array_pop($item);
                $key = array_pop($item);
                $list[] = sprintf('<dt>%s</dt><dd>%s</dd>', $key, $value);
            }
        }

        return sprintf("<dl>%s</dl>", implode("\n", $list));
    }

    public const TABLE_NONE = 'none';
    public const TABLE_INDEXES = 'indexes';
    public const TABLE_FIRST_ROW = 'first_row';
    public static function arrayToTable(array $data, string $headers=self::TABLE_NONE): string
    {
        if (empty($data)) {
            return '';
        }
        $table = [];
        $head = '';
        $rows = array_keys($data);
        if ($headers === self::TABLE_FIRST_ROW) {
            if (count($data) < 2) {
                return '';
            }
            $head = sprintf('<thead><tr><th>%s</th></tr></thead>', implode('</th><th>', $data[$rows[0]]));
            unset($rows[0]);
        }
        elseif ($headers === self::TABLE_INDEXES) {
            if (count($data) < 1) {
                return '';
            }
            $head = sprintf('<thead><tr><th>%s</th></tr></thead>', implode('</th><th>', array_keys($data[$rows[0]])));
        }

        foreach ($rows as $rowIndex) {
            $row = [];
            foreach ($data[$rowIndex] as $key => $value) {
                $row[] = sprintf('<td>%s</td>', $value);
            }
            $table[] = sprintf('<tr>%s</tr>', implode('', $row));
        }

        return sprintf("<table>%s<tbody>%s</tbody></table>", $head, implode("\n", $table));
    }

    public static function secondsToHuman(int $seconds, TranslatorInterface $translator): string
    {
        if ($seconds < 60) {
            $format = 'secs';
        }
        elseif ($seconds < 60*60) {
            $format = 'secs+mins';
        }
        elseif ($seconds < 60*60*48) {
            $format = 'secs+mins+hours';
        }
        elseif ($seconds < 60*60*24*60) {
            $format = 'secs+mins+hours+days';
        }
        elseif ($seconds < 60*60*24*30*12) {
            $format = 'secs+mins+hours+days+months';
        }
        else {
            $format = 'secs+mins+hours+days+months+years';
        }
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        $map = [
            'secs' => '%s',
            'mins' => '%i',
            'hours' => '%h',
            'days' => '%a',
            'months' => '%m',
            'years' => '%y',
        ];
        $fmt = [];
        foreach (explode('+', $format) as $item) {
            $fmt[$item] = sprintf('%s %s', $map[$item], $translator->trans("locale.time.$item"));
        }
        return $dtF->diff($dtT)->format(implode(', ', array_reverse($fmt, true)));
    }
}