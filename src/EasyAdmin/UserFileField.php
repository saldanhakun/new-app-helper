<?php

namespace Saldanhakun\AppHelper\EasyAdmin;

use Saldanhakun\AppHelper\Form\UserFileType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\TextAlign;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Symfony\Component\Security\Core\User\UserInterface;

class UserFileField implements FieldInterface
{

    use FieldTrait;

    public const OPTION_USER_ID = 'userId';

    /**
     * @param string|false|null $label
     */
    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplateName('crud/field/hidden')
            ->setFormType(UserFileType::class)
            ->setFormTypeOption('block_name', "ea_UserFile_field")
            ->addCssClass('field-userfile')
            ->setDefaultColumns('col-md-12 col-xxl-8')
            ->setTextAlign(TextAlign::CENTER);
    }

    /**
     * @param UserInterface|string $user
     * @return $this
     */
    public function setUser($user): self
    {
        if ($user instanceof UserInterface) {
            $this->setCustomOption(self::OPTION_USER_ID, $user->getUserIdentifier());
            $this->setFormTypeOption('user', $user);
        }
        else {
            $this->setCustomOption(self::OPTION_USER_ID, $user);
        }
        return $this;
    }

    public function setMaxSize($max): self
    {
        $this->setFormTypeOption('max_size', $max);
        return $this;
    }

    public function setMimeTypes($mimes): self
    {
        $this->setFormTypeOption('mime_types', $mimes);
        return $this;
    }

    public function setDefaultSource($source): self
    {
        $this->setFormTypeOption('default_source', $source);
        return $this;
    }

    public function setSources(array $sources): self
    {
        $this->setFormTypeOption('sources', $sources);
        return $this;
    }

}