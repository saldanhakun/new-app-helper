<?php

namespace Saldanhakun\AppHelper\EasyAdmin;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField as OriginalTextareaField;

class TextAreaField implements FieldInterface
{

    use FieldTrait;

    /**
     * @param string|false|null $label
     */
    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplateName('crud/field/textarea')
            ->setFormType(TextareaType::class)
            ->addCssClass('field-textarea')
            ->addJsFiles('bundles/easyadmin/form-type-textarea.js')
            ->setDefaultColumns('col-md-9 col-xxl-7')
            ->setMaxLength(255)
            ->setCustomOption(OriginalTextareaField::OPTION_NUM_OF_ROWS, 3)
            ->setCustomOption(OriginalTextareaField::OPTION_RENDER_AS_HTML, false)
            ->setCustomOption(OriginalTextareaField::OPTION_STRIP_TAGS, false);
    }

    /**
     * This option is ignored when using 'renderAsHtml()' to avoid
     * truncating contents in the middle of an HTML tag.
     */
    public function setMaxLength(int $length): self
    {
        if ($length < 1) {
            throw new \InvalidArgumentException(sprintf('The argument of the "%s()" method must be 1 or higher (%d given).', __METHOD__, $length));
        }

        $attr = $this->dto->getFormTypeOption('attr')?:[];
        if ($length > 0) {
            $this->setCustomOption(OriginalTextareaField::OPTION_MAX_LENGTH, $length);
            $attr['maxlength'] = $length;
        }
        else {
            $this->setCustomOption(OriginalTextareaField::OPTION_MAX_LENGTH, null);
            unset($attr['maxlength']);
        }
        $this->setFormTypeOption('attr', $attr);

        return $this;
    }

    public function setNumOfRows(int $rows): self
    {
        if ($rows < 1) {
            throw new \InvalidArgumentException(sprintf('The argument of the "%s()" method must be 1 or higher (%d given).', __METHOD__, $rows));
        }

        $this->setCustomOption(OriginalTextareaField::OPTION_NUM_OF_ROWS, $rows);

        return $this;
    }

    public function renderAsHtml(bool $asHtml = true): self
    {
        $this->setCustomOption(OriginalTextareaField::OPTION_RENDER_AS_HTML, $asHtml);

        return $this;
    }

    public function setPlaceholder(string $value): self
    {
        $attr = $this->dto->getFormTypeOption('attr')?:[];
        if (!empty($value)) {
            $attr['placeholder'] = $value;
        }
        else {
            unset($attr['placeholder']);
        }
        $this->setFormTypeOption('attr', $attr);

        return $this;
    }

    public function stripTags(bool $stripTags = true): self
    {
        $this->setCustomOption(OriginalTextareaField::OPTION_STRIP_TAGS, $stripTags);

        return $this;
    }

}