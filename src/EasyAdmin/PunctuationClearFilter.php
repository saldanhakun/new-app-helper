<?php

namespace Saldanhakun\AppHelper\EasyAdmin;

use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\TextFilterType;

class PunctuationClearFilter implements FilterInterface
{
    use FilterTrait;

    public const PUNCTUATION_CPF = ['.', '-'];
    public const PUNCTUATION_CNPJ = ['.', '-', '/'];
    public const PUNCTUATION_PHONE = ['+', '(', ')', ' ', '-'];

    private $punctuation = [];

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(TextFilterType::class)
            ->setFormTypeOption('translation_domain', 'EasyAdminBundle');
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $alias = $filterDataDto->getEntityAlias();
        $property = $filterDataDto->getProperty();
        $comparison = $filterDataDto->getComparison();
        $parameterName = $filterDataDto->getParameterName();
        $value = $filterDataDto->getValue();

        // Normaliza ambas as partes para apenas números
        $subject = sprintf('%s.%s', $alias, $property);
        if (!empty($this->punctuation)) {
            $unsafe = in_array('%', $this->punctuation, true);
            if ($unsafe) {
                $value = str_replace('%', '¨', $value); // quem usaria ¨ porra?
            }
            foreach ($this->punctuation as $punctuation) {
                $value = str_replace($punctuation, '', $value);
                $subject = "REPLACE($subject, '$punctuation', '')";
            }
            if ($unsafe) {
                $value = str_replace('¨', '%', $value);
            }
        }

        $queryBuilder
            ->andWhere(sprintf('%s %s :%s', $subject, $comparison, $parameterName))
            ->setParameter($parameterName, $value);
    }

    public function cpf(): self
    {
        $this->punctuation = self::PUNCTUATION_CPF;
        return $this;
    }

    public function cnpj(): self
    {
        $this->punctuation = self::PUNCTUATION_CNPJ;
        return $this;
    }

    public function phone(): self
    {
        $this->punctuation = self::PUNCTUATION_PHONE;
        return $this;
    }

    public function custom(array $punctuation): self
    {
        $this->punctuation = $punctuation;
        return $this;
    }
}