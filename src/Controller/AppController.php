<?php

namespace Saldanhakun\AppHelper\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AppController extends AbstractController
{

    use AppContextTrait;

}