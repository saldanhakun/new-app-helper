<?php

namespace Saldanhakun\AppHelper\Controller;

use App\Entity\UserFile;use Saldanhakun\AppHelper\DBAL\FileSourceType;use Saldanhakun\AppHelper\Entity\AppRole;use Symfony\Component\HttpFoundation\Response;use Symfony\Component\HttpFoundation\ResponseHeaderBag;use Symfony\Component\Routing\Annotation\Route;

class UserFileController extends AppController
{

    /**
     * Lida com a requisição de um download de arquivo de usuário.
     * @Route("/download/{id}", name="download", requirements={"id"="^[-a-f0-9]{36}$"})
     * @return Response
     */
    public function download(UserFile $userFile): Response
    {
        $userFileUploader = $this->getUserFileUploader();
        $user = $userFile->getUser($this->getUserLoader());
        if ($userFile->getSource() === FileSourceType::SOURCE_PUBLIC) {
            $path = implode(DIRECTORY_SEPARATOR, [
                $userFileUploader->getTargetDirectory(false, $user),
                $userFile->getPath(),
            ]);
            if (!file_exists($path)) {
                throw $this->createNotFoundException($this->trans("File not found", [], 'validations'));
            }
            return $this->file($path, $userFile->getOriginalFilename(), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        }
        if ($userFile->getSource() === FileSourceType::SOURCE_PRIVATE) {
            if ($this->getUser()->getUserIdentifier() !== $user->getUserIdentifier()) {
                // Simula um arquivo não encontrado, para não vazar nenhuma informação. Só deixa passar se for admin.
                if (!$this->isGranted(AppRole::ROLE_ADMIN)) {
                    throw $this->createNotFoundException($this->trans("File not found", [], 'validations'));
                }
            }
            $this->denyAccessUnlessGranted(AppRole::ROLE_USER);
            $path = implode(DIRECTORY_SEPARATOR, [
                $userFileUploader->getTargetDirectory(true, $user),
                $userFile->getPath(),
            ]);
            if (!file_exists($path)) {
                throw $this->createNotFoundException($this->trans("File not found", [], 'validations'));
            }
            return $this->file($path, $userFile->getOriginalFilename(), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        }
        if ($userFile->getSource() === FileSourceType::SOURCE_EXTERNAL) {
            return $this->redirect($userFile->getPath(), 302);
        }
        if ($userFile->getSource() === FileSourceType::SOURCE_GOOGLE_DRIVE) {
            return $this->redirect($userFile->getPath(), 302);
        }
        throw new \LogicException("Not implemented: {$userFile->getSource()}");
    }
}
