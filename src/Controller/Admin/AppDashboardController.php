<?php

namespace Saldanhakun\AppHelper\Controller\Admin;

use Saldanhakun\AppHelper\Controller\AppContextTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

/**
 * Funcionalidades compartilhadas entre todos os dashboards da Administração
 * @package App\Controller\Admin
 */
abstract class AppDashboardController extends AbstractDashboardController
{

    use AppContextTrait;

    abstract protected function getDashboardName(): string;

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->trans($this->getDashboardName()));
    }

}
