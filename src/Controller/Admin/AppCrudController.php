<?php

namespace Saldanhakun\AppHelper\Controller\Admin;

use Saldanhakun\AppHelper\Controller\AppContextTrait;
use Saldanhakun\AppHelper\Entity\AppEntity;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

abstract class AppCrudController extends AbstractCrudController
{

    use AppContextTrait;

    /**
     * @return string Nome do CRUD na forma singular
     */
    abstract protected function getSingular(): string;

    /**
     * @return string Nome do CRUD na forma plural
     */
    abstract protected function getPlural(): string;

    /**
     * Retorna a configuração de validação para o formulário. Por default, é um único item
     * que vale para todos os formulários, mas é possível retornar 2 itens, para forms de New e Edit.
     * O padrão é validar pelo método validateCrudForm() da entidade.
     * @return array
     */
    protected function getValidationGroups(): array {
        return ['validation_groups' => ['Default', 'validate_crud_form']];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInSingular($this->getSingular())
            ->setEntityLabelInPlural($this->getPlural())
            ->showEntityActionsInlined()
            ->setFormThemes([
                'bundles/EasyAdminBundle/app_form.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig',
            ]);
    }

    /**
     * Geração dos campos do CRUD. Utiliza a implementação automática de ConfigureFields
     * @param string $pageName
     * @return iterable
     */
    abstract public function _configureFields(string $pageName): iterable;

    /**
     * Pós-processamento dos campos. Utiliza um método intermediário (_configureFields)
     * pra permitir a tradução de termos automaticamente.
     * @param string $pageName
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        $fields = iterator_to_array($this->_configureFields($pageName));

        foreach ($fields as $field) {
            /** @var Field $field */

            $ph = $field->getAsDto()->getFormTypeOption('placeholder');
            if (!empty($ph)) {
                $field->getAsDto()->setFormTypeOption('placeholder', $this->trans($ph));
            }

            $attr = $field->getAsDto()->getFormTypeOption('attr');
            if (!empty($attr)) {
                if (!empty($attr['placeholder'])) {
                    $attr['placeholder'] = $this->trans($attr['placeholder']);
                    $field->getAsDto()->setFormTypeOption('attr', $attr);
                }
                if (!empty($attr['title'])) {
                    $attr['title'] = $this->trans($attr['title']);
                    $field->getAsDto()->setFormTypeOption('attr', $attr);
                }
            }

        }
        return $fields;
    }

    /**
     * Nas páginas onde isso faz algum sentido, retorna o objeto atual
     * @param string $pageName
     * @return AppEntity|null
     */
    protected function getCurrentEntity(string $pageName): ?AppEntity
    {
        $current = null;
        if (in_array($pageName, [Crud::PAGE_DETAIL, Crud::PAGE_EDIT], true)) {
            $current = $this->getContext()->getEntity()->getInstance();
        }
        return $current;
    }

    /**
     * @param string $key nome do filtro
     * @return string|null
     */
    protected function getFilterValue(string $key): ?string
    {
        $filter_value = null;
        $request = $this->getContext()
            ? $this->getContext()->getRequest()
            : $this->get('request_stack')->getMainRequest();
        $filters = $request === null ? null : $request->get('filters');
        if (is_array($filters) && array_key_exists($key, $filters)) {
            $filter = $filters[$key];
            if (is_scalar($filter)) {
                // filtros simples, como booleanos por ex
                $filter_value = (string) $filter;
            }
            elseif (array_key_exists('comparison', $filter) && $filter['comparison'] === '=') {
                // filtros complexos, só considera quando o valor é específico, e não parcial ou comparativo
                $filter_value = (string) $filter['value'];
            }
        }
        return $filter_value;
    }

    /**
     * Verifica se a tela está filtrada por um filtro específico
     * @param string $key
     * @param mixed $value Se null, considera qualquer valor
     * @return bool|null Se null, indica que não há um filtro
     */
    protected function isFilteredBy(string $key, ?string $value=null): ?bool
    {
        $filter_value = $this->getFilterValue($key);
        $filtered = null;
        if ($filter_value !== null) {
            $filtered = $value === null || $value === $filter_value;
        }
        return $filtered;
    }

}
