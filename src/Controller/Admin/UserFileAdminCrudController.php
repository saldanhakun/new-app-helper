<?php

namespace Saldanhakun\AppHelper\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\NullFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use Saldanhakun\AppHelper\DBAL\FileSourceType;
use Saldanhakun\AppHelper\EasyAdmin\DisplayField;
use Saldanhakun\AppHelper\Entity\AppEntity as UserFile;

abstract class UserFileAdminCrudController extends AppCrudController
{

    protected function getPlural(): string
    {
        return "userFile.plural";
    }

    protected function getSingular(): string
    {
        return "userFile.singular";
    }

    public function _configureFields(string $pageName): iterable
    {
        $translationPrefix = 'userFile';
        /** @var UserFile $currentFile */
        $currentFile = $this->getCurrentEntity($pageName);
        $isBackup = $currentFile && $currentFile->getBackupOf();
        UserFile::setTranslator($this->getTranslator());

        yield TextField::new('id', "$translationPrefix.id")
            ->setDisabled(true)
            ->hideWhenCreating();

        $loader = $this->getUserProfileRepository();
        yield DisplayField::new('userId', "$translationPrefix.user")
            ->setDisplayCallback(function ($userId, UserFile $userFile) use ($loader) {
                $user = $userFile->getUser($loader);
                $info = [
                    [
                        $this->trans('userProfile.displayNameLabel'),
                        $user->getDisplayName(),
                    ],
                    [
                        $this->trans('userProfile.emailLabel'),
                        $user->getEmail(),
                    ],
                ];
                return DisplayField::arrayToDL($info);
            })
            ->setDisabled(true);

        yield ChoiceField::new('source', "$translationPrefix.source")
            ->setChoices(FileSourceType::getChoices())
            ->setDisabled(true);

        /** Campos ajustáveis pelo admin */
        yield FormField::addPanel("$translationPrefix.tuningPanel");

        yield TextField::new('title', "$translationPrefix.title")
            ->setDisabled($isBackup);
        yield TextField::new('originalFilename', "$translationPrefix.originalFilename")
            ->setDisabled($isBackup);
        yield BooleanField::new('isFinal', "$translationPrefix.isFinal")
            ->setDisabled($isBackup)
            ->hideOnIndex();
        if ($isBackup || $this->isFilteredBy('backupOf', 'not_null')) {
            yield AssociationField::new('backupOf', "$translationPrefix.backupOf")
                ->formatValue(function ($value, UserFile $entity) {
                    return $entity->getBackupOf()->getId();
                })
                ->setDisabled(true);
        }
        if (!$isBackup) {
            yield CollectionField::new('backups', "$translationPrefix.backups")
                ->setDisabled(true)
                ->allowDelete(false)
                ->allowAdd(false)
                ->hideOnIndex();
        }

        /** Propriedades do Arquivo */
        yield FormField::addPanel("$translationPrefix.propertiesPanel");

        yield DisplayField::new('fileSize', "$translationPrefix.fileSize")
            ->hideOnIndex()
            ->withInput()
            ->setDisplayProperty('fileSizeHuman');
        yield DateTimeField::new('fileDate', "$translationPrefix.fileDate")
            ->hideOnIndex()
            ->setDisabled(true);
        yield TextField::new('fileType', "$translationPrefix.fileType")
            ->hideOnIndex()
            ->setDisabled(true);
        $uploader = $this->getUserFileUploader();
        yield DisplayField::new('path', "$translationPrefix.path")
            ->hideOnIndex()
            ->setDisplayCallback(function ($path, UserFile $userFile) use ($uploader) {
                if (FileSourceType::isLocal($userFile->getSource())) {
                    $url = $this->generateUrl('download', ['id' => $userFile->getId()]);
                    $info = [
                        [
                            $this->trans('userFile.localRepositoryLabel'),
                            $uploader->getTargetDirectory($userFile->getSource() === FileSourceType::SOURCE_PRIVATE, $userFile->getUser()),
                        ],
                        [
                            $this->trans('userFile.downloadLabel'),
                            sprintf('<a href="%s" target="_blank">%s</a>', $url, $userFile->getPath()),
                        ],
                    ];
                } else {
                    $info = [
                        [
                            $this->trans('userFile.remoteDomainLabel'),
                            parse_url($userFile->getPath(), PHP_URL_HOST),
                        ],
                        [
                            $this->trans('userFile.remoteLinkLabel'),
                            sprintf('<a href="%s" target="_blank">%s</a>', $userFile->getPath(), $userFile->getPath()),
                        ],
                    ];
                }
                return DisplayField::arrayToDL($info);
            })
            ->setDisplayAsHtml()
            ->setDisabled(true);
        yield TextField::new('remoteKey', "$translationPrefix.remoteKey")
            ->hideOnIndex()
            ->setDisabled(true);

        yield DateTimeField::new('createdAt', "$translationPrefix.createdAt")
            ->setDisabled(true);
    }

    public function configureFilters(Filters $filters): Filters
    {
        $translationPrefix = 'userFile';
        $filters = parent::configureFilters($filters)
            ->add(ChoiceFilter::new('source')
                ->setLabel("$translationPrefix.sourceFilter")
                ->setChoices(FileSourceType::getTranslatedChoices($this->getTranslator()))
            )
            ->add(EntityFilter::new('user')
                ->setLabel("$translationPrefix.userFilter")
            )
            ->add(BooleanFilter::new('isFinal')
                ->setLabel("$translationPrefix.isFinalFilter")
            )
            ->add(NullFilter::new('backupOf')
                ->setLabel("$translationPrefix.backupFilter.label")
                ->setChoiceLabels("$translationPrefix.backupFilter.null", "$translationPrefix.backupFilter.notnull")
            )
            ->add(TextFilter::new('path')
                ->setLabel("$translationPrefix.pathFilter")
            )
            ->add(TextFilter::new('id')
                ->setLabel("$translationPrefix.idFilter")
            );
        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->remove(Crud::PAGE_INDEX, Action::NEW);
        return parent::configureActions($actions);
    }
}