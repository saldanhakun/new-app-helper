<?php

namespace Saldanhakun\AppHelper\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Saldanhakun\AppHelper\Entity\UserProfile;
use Saldanhakun\AppHelper\Repository\UserProfileRepository;
use Saldanhakun\AppHelper\Service\UserFileUploader;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Funcionalidades compartilhadas entre todos os dashboards e Cruds da Administração
 * @package App\Controller\Admin
 */
trait AppContextTrait
{
    private $translator;
    private $entityManager;
    private $userFileUploader;
    private $userProfileRepository;

    public function __construct(TranslatorInterface $translator, EntityManagerInterface $entityManager, UserFileUploader $userFileUploader, UserProfileRepository $userProfileRepository)
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->userFileUploader = $userFileUploader;
        $this->userProfileRepository = $userProfileRepository;
    }

    protected function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * @return string Domínio para tradução do controller
     */
    protected function getTranslationDomain(): string
    {
        return 'messages';
    }

    /**
     * @param string $message Mensagem traduzível
     * @param array $params Parâmetros atribuídos
     * @return string mensagem traduzida
     */
    protected function trans(string $message, array $params=[]) {
        return $this->translator->trans($message, $params, $this->getTranslationDomain());
    }

    /**
     * Retorna uma versão traduzida do array, com as chaves intactas
     * @param $array
     * @param array $params
     * @return array
     */
    public function transArray($array, array $params=[]): array
    {
        $translated = [];
        foreach ($array as $key => $value) {
            $translated[$key] = $this->trans($value, $params);
        }
        return $translated;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return UserFileUploader
     */
    protected function getUserFileUploader(): UserFileUploader
    {
        return $this->userFileUploader;
    }

    /**
     * @return UserLoaderInterface
     */
    protected function getUserProfileRepository(): UserProfileRepository
    {
        return $this->userProfileRepository;
    }

    protected function getUserProfile(): UserProfile
    {
        return $this->userProfileRepository->findUser($this->getUser()->getUserIdentifier());
    }

}
