<?php

namespace Saldanhakun\AppHelper\Repository;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Doctrine\ORM\EntityManagerInterface;
use Saldanhakun\AppHelper\Entity\UserProfile;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserProfileRepository implements UserLoaderInterface
{
    private $conn;
    private $casDomain;
    private $casToken;
    private $casKey;
    private $casAuthorization;
    private $em;
    private $urlGenerator;
    private $userCache = [];

    public function __construct($casDomain, $casToken, $casSecret, EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator)
    {
        $this->conn = new CurlHttpClient();
        $this->casDomain = rtrim($casDomain, '/');
        $this->casToken = $casToken;
        $this->casKey = Key::loadFromAsciiSafeString($casSecret);
        $this->em = $entityManager;
        $this->urlGenerator = $urlGenerator;
    }

    protected function _request(string $method, string $entryPoint, array $payload=[], array $headers=[]): array
    {
        try {
            $headers['X-ORIGIN'] = $this->urlGenerator->generate('homepage', [], UrlGenerator::ABSOLUTE_URL);
            if ($entryPoint !== 'authorize') { // Caso especial, é o único entryPoint que não depende de uma autorização prévia.
                $this->_checkAuthorization();
                $headers['X-CAS-TOKEN'] = $this->casToken;
                $headers['X-CAS-AUTH'] = Crypto::encrypt(json_encode(array_merge(
                    $this->casAuthorization,
                    ['random' => uniqid('', true)]
                )), $this->casKey);
            }
            $url = sprintf('%s/%s', $this->casDomain, trim($entryPoint, '/'));

            $response = $this->conn->request($method, $url, [
                $method === 'GET' ? 'query' : 'json' => $payload,
                'headers' => $headers,
            ]);
            $type = $response->getHeaders()['content-type'][0];
            $data = [
                'statusCode' => $response->getStatusCode(),
                'content' => $type === 'application/json' ? $response->toArray() : $response->getContent(),
            ];
            return $data;
        } catch (\Exception $e) {
            if (isset($response)) dump($response);
            throw $e;
            return [
                'statusCode' => 500,
                'error' => $e->getMessage(),
            ];
        }
    }
    protected function _get(string $entryPoint, array $payload=[], array $headers=[]): array
    {
        return $this->_request('GET', $entryPoint, $payload, $headers);
    }
    protected function _post(string $entryPoint, array $payload=[], array $headers=[]): array
    {
        return $this->_request('POST', $entryPoint, $payload, $headers);
    }
    protected function _put(string $entryPoint, array $payload=[], array $headers=[]): array
    {
        return $this->_request('PUT', $entryPoint, $payload, $headers);
    }
    protected function _delete(string $entryPoint, array $payload=[], array $headers=[]): array
    {
        return $this->_request('DELETE', $entryPoint, $payload, $headers);
    }
    protected function _patch(string $entryPoint, array $payload=[], array $headers=[]): array
    {
        return $this->_request('PATCH', $entryPoint, $payload, $headers);
    }

    protected function _assertStatus($response, int $status=200, bool $dryRun=false)
    {
        try {
            if (!$response || !is_array($response) || !array_key_exists('statusCode', $response)) {
                throw new InvalidArgumentException("User CAS: not a response");
            }
            if ($response['statusCode'] === 404 && 404 !== $status) {
                throw new NotFoundHttpException("User CAS: Not Found");
            }
            if ($response['statusCode'] === 403 && 403 !== $status) {
                throw new UnauthorizedHttpException("User CAS: Not Authorized");
            }
            if ($response['statusCode'] !== $status) {
                throw new HttpException($response['statusCode'], "User CAS: unexpected status: {$response['statusCode']} (expecting $status)");
            }
        }
        catch (\Exception $e) {
            if ($dryRun) {
                return false;
            }
            throw $e;
        }
        return true;
    }

    protected function _assertValue(array $response, string $field, $validator='exists')
    {
        if (!array_key_exists('content', $response)) {
            throw new \LogicException("Invalid response");
        }
        if ($field === '') {
            // Interesse no conteúdo como um todo
            $exists = !empty($response['content']);
        }
        else {
            // Interesse numa parte do conteúdo
            $exists = is_array($response['content']) && array_key_exists($field, $response['content']);
        }
        if ($validator === 'exists') {
            if (!$exists) {
                throw new InvalidArgumentException("Value not found: $field");
            }
            return $field === '' ? $response['content'] : $response['content'][$field];
        }
        if ($validator === 'not_exists') {
            if ($exists) {
                throw new InvalidArgumentException("Value found: $field");
            }
            return null;
        }
        if (is_callable($validator)) {
            return $validator($response['content'], $field);
        }
        throw new \LogicException("Invalid validator: $validator");
    }

    protected function _authorize()
    {
        $response = $this->_get('authorize', [
            'token' => $this->casToken,
        ]);
        $this->_assertStatus($response);
        $auth = $this->_assertValue($response, 'authorization');
        $this->casAuthorization = json_decode(Crypto::decrypt($auth, $this->casKey), true);
    }

    protected function _checkAuthorization()
    {
        if (!empty($this->casAuthorization)) {
            if ($this->casAuthorization['expires'] >= time()) {
                $this->casAuthorization = null;
            }
        }
        if (empty($this->casAuthorization)) {
            $this->_authorize();
        }
    }

    public function findUserBy($field, $value): ?UserProfile
    {
        $response = $this->_get('user/fetch', [
            'key' => $field,
            'value' => $value,
        ]);
        if ($response['statusCode'] === 404 || $response['statusCode'] === 410) {
            return null;
        }
        $this->_assertStatus($response);
        return new UserProfile($this->_assertValue($response, ''), $this->em);
    }

    public function findUser($id): UserProfile
    {
        if (!array_key_exists($id, $this->userCache)) {
            $this->userCache[$id] = $this->findUserBy('id', $id);
        }
        $user = $this->userCache[$id];
        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }
        return $user;
    }

    public function loadUserByUsername($username): UserInterface
    {
        return $this->loadUserByIdentifier($username);
    }
    public function loadUserByIdentifier($identifier): UserInterface
    {
        return $this->findUserBy('id', $identifier);
    }

}
