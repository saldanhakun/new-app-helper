# README #

Welcome to New App Helper! 

## What is this repository for? ##

For now, description only in Brazilian Portuguese:

Este repositório é uma tentativa minha de organizar pequenas construções
e códigos que acabo utilizando em pratiamente todo novo projeto meu. A
ideia é que incluindo essa dependência em novos projetos, a configuração
fica mais fácil.

Por hora é tudo muito cru e ainda requer muita ação manual, mas pretendo
mais tarde criar uma regra Flex para cuidar por exemplo dos acertos em
arquivos de configuração, e também alguns comandos make:xxx para ajudar.